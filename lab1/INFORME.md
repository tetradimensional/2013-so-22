Baash: informe
==============

> _Grupo ?:_ Ledesma Noelia, Mancera Rodrigo, Godoy Lucas  
> Sistemas Operativos 2013  
> FaMAF - Univ. Nacional de Cordoba - Argentina  

Introduccion
------------

El proyecto de laboratorio numero 1 consistio en implementar un shell basico siguiendo el standard de UNIX.  
El shell en cuestion (denominado baash) deberia ser capaz de ejecutar comandos ingresados por el usuario, redireccionar la salida standard a un archivo y/o tomar por entrada standard datos de otro archivo. Ademas tambien se prevee la comunicacion entre comandos mediante un pipe de redireccion.  
A continuacion se describe el proceso de implementacion.

Herramientas usadas
--------------------

Las herramientas usadas son las que fueron otorgadas o sugeridas por los docentes. Estas incluyen el lenguaje de programacion C (junto con su libreria standard), la libreria bstring, la libreria GLib, los modulos lexer y parser (otorgados por los docentes), GNU make, Valgrind y GDB. A su vez los docentes entregaron 'unit tests' implementados con la herramienta 'check' que usamos para asegurar que el programa sea correcto.

Aspectos de la implementacion
--------------------

> ### Pipes
> Se implemento baash para n pipes. Pudiendo soportar varios comandos encadenados por pipes.  
> En un principio se penso en crear la cantidad necesaria de pipes antes de crear todos los procesos. Pero este metodo era muy
> molesto y costoso a la hora de cerrar todos los otros pipes no usados por cada proceso hijo. (Ya que, el ser creados los pipes
> en el procesos padre, cada uno de los hijos hereda copias de lo mismos.)
> Investigando un poco por internet se encontro un pseudocodigo [1] el cual daba una idea de tener solo 2 pipes, uno viejo y uno
> nuevo, e ir 'rotando' entre ellos. Decidimos adaptar esa solucion ya que es mas eficiente y sencilla.

> ### Mensajes de error
> Se trato de imitar el comportacmiento de GNU bash en los mensajes.
> Se implemento la funcion `prettyerr()` la cual sirve de 'wrapper' a la funcion `perror()`. El objetivo de esta es poder
> formatear una cadena de caracteres para despues pasarsela a `perror()`. Ya que esta ultima funcion no acepta codigo formateado
> solo cadenas. Para lo cual se utilo la libreria bstring para facil manipulacion, aunque tambien se podria haber usado el
> modulo `<string.h>` de C.

Problemas encontrados
---------------------

> ### Memory Leaks
> Tanto memory leaks como lecturas o escrituras invalidas fueron encontradas durante la prueba del programa. Todos fueron solucionados usando la herramienta valgrind. Sin embargo quedaron leaks relacionados con la libreria GLib que no pudimos rastrear y quedaron sin resolver.

> ### Procesos
> Uno de los problemas mas importantes es que estabamos creando nuevos procesos por cada <enter> (comando vacio) lo cual no solo
> gastaba mas recursos sino que ademas alteraba los casos de pruebas para los procesos zombies. Esto se debe a que por cada
> comando vacio, al no tener '&', el shell tenia que esperarlo a que terminara.

> ### Procesos 'Zombies'
> Para esta parte se trato de simular lo mas posible el estilo implementado en GNU bash.
> En general se modifico varias veces la posicion y forma de la llamada a `waitpid()` hasta comprender que estaba pasando. Ultimamente se opto por llamar al siguiente bucle en cada iteracion del programa principal:  

              while (waitpid (-1, &cstatus, WNOHANG) > 0);

> El cual cierra todo los procesos zombies que sean posibles. Asi, si por ejemplo, se lanza el comando `firefox | evince&` y uno de ellos
> (o los 2) finaliza, este quedara en modo zombie hasta que se llame al siguiente comando (cualquiera) y el loop de arriba lo limpie.
> En este sentido no se pudo simular el comportamiento de GNU bash el cual cuando uno de los comandos simples de arriba finaliza es automaticamente limpiado.
> Por otro lado si se parece bastante a la forma de manejar los procesos zombies de dash (Debian Alchimist shell).

A tener en cuenta
-----------------

+ Notar que al llamar `g_queue_pop_head ()` hasta que la lista este vacia devuelve `NULL`, habiendo destruido la lista.
+ Los elementos de la lista en la funcion (almacenados en la variable `current`) `pipeline_to_string` no deben ser destruidos, puesto que de lo conrario surge un Segmentation Fault.
+ El segundo argumento de la funcion `close_pipe` en execute.c se usa para propositos de debugging.

Otras cosas
-----------------

> ### Debugging
> Se utilizo mucho los mensajes de error leidos por la funcion `perror()` y llamadas a `exit()` para encontrar bugs y fallas
> en la implementacion. Tambien se uso GDB aunque no tanto como en otros proyectos ya que el llamar a `fork()` para crear un
> nuevo hijo GDB continua la execucion del padre sin poder ver el comportamiento del hijo.

> ### Mensaje de bienvenida
> Se opto por crear un mensaje de bienvenida para cuando se inicia el baash (solo porque si :).
> Por lo cual el archivo `welcome` que contiene el mensaje es necesario. En caso de no encontrar el archivo
> el programa solo muestra un mensaje de error y continua su ejecucion.

Bibliografia
-----------------

[1] http://stackoverflow.com/questions/916900/having-trouble-with-fork-pipe-dup2-and-exec-in-c/
