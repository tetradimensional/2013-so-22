#include <assert.h>
#include <errno.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <unistd.h>

#include "builtin.h"
#include "command.h"
#include "execute.h"
#include "tests/syscall_mock.h"


static char **cmd_to_array (scommand *sc);
static bool cmd_is_first (int pllen, int cmdno);
static bool cmd_is_last (int pllen, int cmdno);
static void closepipe (int *pipefd, char *msg);
static void prettyerr (char *msg);


void execute_pipeline (pipeline *apipe) {
/*
 * Ejecuta un pipeline, identificando comandos internos, forkeando, y
 *   redirigiendo la entrada y salida. puede modificar `apipe' en el proceso
 *   de ejecución.
 *   apipe: pipeline a ejecutar
 * Requires: apipe!=NULL
 */
    int oldpipe[2];
    int newpipe[2];
    pid_t cpid;
    int cstatus = 0;
    scommand *sc = NULL;
    char **argv = NULL;
    const_bstring redir_in = NULL;
    const_bstring redir_out = NULL;
    int redir_infd = 0;
    int redir_outfd = 0;
    int pllen = 0;
    int cmdno = 0;
    int i = 0;
    assert (apipe != NULL);

    pllen = pipeline_length (apipe);
    while (!pipeline_is_empty (apipe)) {
        sc = pipeline_front (apipe);
        cmdno++;

        scommand_to_string (sc);

        if (!cmd_is_last (pllen, cmdno)) {
            if (pipe (newpipe) == -1) {
                prettyerr ("create newpipe");
            }
        }

        if (command_is_builtin (sc)) {
            builtin_execute_command (sc);
        } else if (scommand_is_empty (sc)) {
            pllen--;  /* don't wait for him at the end */
        } else {
            cpid = fork ();
            if (cpid == -1) {
                prettyerr ("fork");
            }

            /* children part */
            if (cpid == 0) {

                /* redirect stdin */
                redir_in = scommand_get_redir_in (sc);
                if (redir_in != NULL) {
                    redir_infd = open ((char *) redir_in->data, O_RDONLY, 0);
                    if (redir_infd == -1) {
                        prettyerr ((char *) redir_in->data);
                    }
                    dup2 (redir_infd, STDIN_FILENO);
                    if (close (redir_infd) == -1) {
                        prettyerr ("close redirin");
                    }
                }

                /* decide if to use pipe in */
                if (!cmd_is_first (pllen, cmdno)) {
                    if (redir_in == NULL) {
                        dup2 (oldpipe[0], STDIN_FILENO);
                    }
                    closepipe (oldpipe, "close oldpipe");
                }

                /* redirect stdout */
                redir_out = scommand_get_redir_out (sc);
                if (redir_out != NULL) {
                    redir_outfd = open ((char *) redir_out->data,
                                        O_WRONLY | O_CREAT,
                                        S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH);
                    dup2 (redir_outfd, STDOUT_FILENO);
                    if (close (redir_outfd) == -1) {
                        prettyerr ("close redirout");
                    }
                }

                /* decide if to use pipe out */
                if (!cmd_is_last (pllen, cmdno)) {
                    if (redir_out == NULL) {
                        dup2 (newpipe[1], STDOUT_FILENO);
                    }
                    closepipe (newpipe, "close newpipe");
                }

                /* fetch args and execute */
                argv = cmd_to_array (sc);
                if (execvp (argv[0], argv) == -1) {
                    prettyerr (argv[0]);
                }

            /* fathers part */
            } else {

                /* close old pipe */
                if (!cmd_is_first (pllen, cmdno)) {
                    closepipe (oldpipe, "close oldpipe (father)");
                }

                /* update old pipe with new pipe */
                if (!cmd_is_last (pllen, cmdno)) {
                    oldpipe[0] = newpipe[0];
                    oldpipe[1] = newpipe[1];
                }
            }
        }

        pipeline_pop_front (apipe);
    }

    /* wait for childrens to finish if we have to */
    if (pipeline_get_wait (apipe)) {
        for (i = 0; i < pllen; i++) {
            wait (&cstatus);
        }
    }

}

static char **cmd_to_array (scommand *sc) {
/*
 * Returns: un array con los argumentos del commando simple 'sc'.
 * Warning: modifica al comando simple 'sc'.
 * El llamador se apropia de la referencia y deberia liberarla.
 *   sc: simple command
 * Requires: sc != NULL
 */
    int i = 0;
    char **argv = NULL;
    int sclength = 0;
    bstring btmp = NULL;
    assert (sc != NULL);

    sclength = scommand_length (sc);
    argv = calloc (sclength + 1, sizeof (char *));  /* last pos for NULL */

    for (i = 0; i < sclength; i++) {
        btmp = bstrcpy (scommand_front (sc));
        argv[i] = (char *) btmp->data;
        scommand_pop_front (sc);
    }
    argv[sclength] = (char *) NULL;  /* arg list must be NULL terminated */

    return argv;
}

static bool cmd_is_first (int pllen, int cmdno) {
/*
 * Returns: ¿Es cmd el primer comando?
 *   pllen: pipe line length
 *   cmdno: command number
 */
    return cmdno == 1;
}

static bool cmd_is_last (int pllen, int cmdno) {
/*
 * Returns: ¿Es cmd el ultimo comando?
 *   pllen: pipe line length
 *   cmdno: command number
 */
    return cmdno == pllen;
}

static void closepipe (int *pipefd, char *msg) {
/*
 * Cierra ambos file descriptors del pipe y en caso de error llama
 * a la funcion prettyerr(msg)
 *   pipefd: pipe a cerrar
 *   msg: eiqueta a incluir en el mensaje
 * Requires: pipefd != NULL
 * Warning: puede llamar a exit() en caso de encontrar errores.
 */
    assert (pipefd != NULL);

    if (close (pipefd[0]) == -1) {
        prettyerr (msg);
    }

    if (close (pipefd[1]) == -1) {
        prettyerr (msg);
    }
}

static void prettyerr (char *msg) {
/*
 * Imprime un mensaje de error formateado de la forma:
 *   baash: <msg>: <errmsg>
 * donde errmsg es el error encontrado en el sistema,
 * y llama a la funcion exit con codigo EXIT_FAILURE.
 *   msg: eitqueta a incluir en el mensaje
 * Requires: msg != NULL
 */
    bstring errmsg = NULL;
    assert (msg != NULL);

    errmsg = bformat ("baash: %s", msg);
    perror ((char *) errmsg->data);
    bdestroy (errmsg);
    errmsg = NULL;
    exit (EXIT_FAILURE);
}
