#include <assert.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include "bstring/bstrlib.h"
#include "builtin.h"
#include "command.h"
#include "tests/syscall_mock.h"


bool is_cd (scommand *sc) {
/*
 * Indica si el comando simple es el comando builtin cd.
 *   sc: comando simple
 * Returns: ¿Es el builtin cd?
 * Requires: sc != NULL
 */
    assert (sc != NULL);

    return !scommand_is_empty (sc) && biseqcstr (scommand_front (sc), "cd");
}

bool is_exit (scommand *sc) {
/*
 * Indica si el comando simple es el comando builtin exit.
 *   sc: comando simple
 * Returns: ¿Es el builtin exit?
 * Requires: sc != NULL
 */
    assert (sc != NULL);

    return !scommand_is_empty (sc) && biseqcstr (scommand_front (sc), "exit");
}

bool command_is_builtin (scommand *sc) {
/*
 * Indica si el comando simple es un comando builtin.
 * Los comandos bultins son: {cd, exit}
 *   sc: comando simple a decidir si es comando builtin.
 * Returns: ¿Es un comando builtin?
 * Requires: sc != NULL
 */
    bool result = false;
    assert (sc != NULL);

    if (!scommand_is_empty (sc)) {
        result = is_cd (sc) || is_exit (sc);
    }

    return result;
}

void builtin_execute_command (scommand *sc) {
/*
 * Ejecuta comandos simples que sean builtins.
 * Los comandos bultins son: {cd, exit}
 *   sc: comando simple a ejecutar
 * Requires: sc != NULL && command_is_builtin (sc)
 */
    bstring path = NULL;
    bstring errmsg = NULL;
    assert (sc != NULL);
    assert (command_is_builtin (sc));

    if (is_cd (sc)) {
        if (scommand_length (sc) == 1) {
            path = bfromcstr (getenv ("HOME"));
        } else {
            scommand_pop_front (sc);
            path = bstrcpy ((bstring) scommand_front (sc));
        }
        if (chdir ((char *) path->data) == -1) {
            errmsg = bfromcstr ("baash: cd: ");
            bconcat (errmsg, path);
            perror ((char *) errmsg->data);

            bdestroy (errmsg);
            errmsg = NULL;
        }

        bdestroy (path);
        path = NULL;
    }

    if (is_exit (sc)) {
        exit (EXIT_SUCCESS);
    }
}
