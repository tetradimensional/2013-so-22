#ifndef BUILTIN_H
#define BUILTIN_H

#include <stdbool.h>

#include "command.h"


bool is_cd (scommand *sc);
/*
 * Indica si el comando simple es el comando builtin cd.
 *   sc: comando simple
 * Returns: ¿Es el builtin cd?
 * Requires: sc != NULL
 */

bool is_exit (scommand *sc);
/*
 * Indica si el comando simple es el comando builtin exit.
 *   sc: comando simple
 * Returns: ¿Es el builtin exit?
 * Requires: sc != NULL
 */

bool command_is_builtin (scommand *sc);
/*
 * Indica si el comando simple es un comando builtin.
 * Los comandos bultins son: {cd, exit}
 *   sc: comando simple a decidir si es comando builtin.
 * Returns: ¿Es un comando builtin?
 * Requires: sc != NULL
 */

void builtin_execute_command (scommand *sc);
/*
 * Ejecuta comandos simples que sean builtins.
 * Los comandos bultins son: {cd, exit}
 *   sc: comando simple a ejecutar
 * Requires: sc != NULL && command_is_builtin (sc)
 */

#endif /* BUILTIN_H */
