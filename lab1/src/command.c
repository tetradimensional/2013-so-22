/* A partir de man bash, en su sección de SHELL GRAMMAR,
 * se diseñaron dos TAD scommand (comando simple) y
 * pipeline (secuencia de comandos simples separados por
 * pipe).
 */

#include <assert.h>
#include <glib.h>
#include <stdbool.h>
#include <stdlib.h>

#include "bstring/bstrlib.h"  /* las *_to_string se dan en bstring */
#include "command.h"

#define PIPE_CHAR '|'
#define REDIR_IN_CHAR '<'
#define REDIR_OUT_CHAR '>'
#define WAIT_SYMBOL '&'
#define WHITE_SPACE_CHAR ' '


/* scommand: comando simple.
 * Ejemplo: ls -l ej1.c > out < in
 * Se presenta como una secuencia de cadenas donde la primera se denomina
 * comando y desde la segunda se denominan argumentos.
 * Almacena dos cadenas que representan los redirectores de entrada y salida.
 * Cualquiera de ellos puede estar NULL indicando que no hay redirección.
 *
 * En general, todas las operaciones hacen que el TAD adquiera propiedad de
 * los argumentos que le pasan. Es decir, el llamador queda desligado de la
 * memoria utilizada, y el TAD se encarga de liberarla.
 *
 * Externamente se presenta como una secuencia de strings donde:
 *           _________________________________
 *  front -> | cmd | arg1 | arg2 | ... | argn | <-back
 *           ---------------------------------
 *
 * La interfaz es esencialmente la de una cola. A eso se le
 * agrega dos accesores/modificadores para redirección de entrada y salida.
 */


/* Simple Command private structure */
struct scommand_s {
    GQueue *cmd;
    bstring out;
    bstring in;
};


scommand *scommand_new (void) {
/*
 * Nuevo `scommand', sin comandos o argumentos y los redirectores vacíos
 * Returns: nuevo comando simple sin ninguna cadena y redirectores vacíos.
 * Ensures: result != NULL && scommand_is_empty (result) &&
 * scommand_get_redir_in (result) == NULL &&
 * scommand_get_redir_out (result) == NULL
 */
    scommand *new_scommand = NULL;

    new_scommand = calloc (1, sizeof (*new_scommand));
    new_scommand->cmd = g_queue_new ();
    new_scommand->in = NULL;
    new_scommand->out = NULL;
    assert (new_scommand != NULL && scommand_is_empty (new_scommand) &&
            scommand_get_redir_in (new_scommand) == NULL &&
            scommand_get_redir_out (new_scommand) == NULL);

    return new_scommand;
}

void scommand_destroy (scommand *self) {
/*
 * Destruye `self'.
 * self: comando simple a destruir.
 * Requires: self != NULL
 */
    assert (self != NULL);
    if (self->cmd != NULL)
        g_queue_free_full (self->cmd, (GDestroyNotify) bdestroy);
    self->cmd = NULL;

    bdestroy (self->out);
    bdestroy (self->in);
    self->in = NULL;
    self->out = NULL;

    free (self);
    self = NULL;
}

/* Modificadores */

void scommand_push_back (scommand *self, bstring argument) {
/*
 * Agrega por detrás una cadena a la secuencia de cadenas.
 * self: comando simple al cual agregarle la cadena.
 * argument: cadena a agregar. El TAD se apropia de la referencia.
 * Requires: self!=NULL && argument!=NULL
 * Ensures: !scommand_is_empty()
 */
    assert (self != NULL);
    assert (argument != NULL);

    g_queue_push_tail (self->cmd, argument);

    assert (!scommand_is_empty (self));
}

void scommand_pop_front (scommand *self) {
/*
 * Quita la cadena de adelante de la secuencia de cadenas.
 * self: comando simple al cual sacarle la cadena del frente.
 * Requires: self!=NULL && !scommand_is_empty(self)
 */
    gpointer str = NULL;

    assert (self != NULL);
    assert (!scommand_is_empty (self));

    str = g_queue_pop_head (self->cmd);
    bdestroy (str);
    str = NULL;
}

void scommand_set_redir_out (scommand *self, bstring filename) {
/*
 * Define la redirección de entrada (salida).
 * self: comando simple al cual establecer la redirección de entrada (salida)
 * filename: cadena con el nombre del archivo de la redirección
 * o NULL si no se quiere redirección. El TAD se apropia de la referencia.
 * Requires: self!=NULL
 */
    assert (self != NULL);
    if (self->out != NULL) {
        bdestroy (self->out);
        }
    self->out = filename;
}

void scommand_set_redir_in (scommand *self, bstring filename) {
/*
 * Define la redirección de entrada (salida).
 * self: comando simple al cual establecer la redirección de entrada (salida)
 * filename: cadena con el nombre del archivo de la redirección
 * o NULL si no se quiere redirección. El TAD se apropia de la referencia.
 * Requires: self!=NULL
 */
    assert (self != NULL);
    if (self->out != NULL) {
        bdestroy (self->in);
    }
    self->in = filename;
}

/* Proyectores */

bool scommand_is_empty (const scommand *self) {
/*
 * Indica si la secuencia de cadenas tiene longitud 0.
 * self: comando simple a decidir si está vacío.
 * Returns: ¿Está vacío de cadenas el comando simple?
 * Requires: self!=NULL
 */
    assert (self != NULL);

    return (0 == g_queue_get_length (self->cmd));
}

unsigned int scommand_length (const scommand *self) {
/*
 * Da la longitud de la secuencia cadenas que contiene el comando simple.
 * self: comando simple a medir.
 * Returns: largo del comando simple.
 * Requires: self!=NULL
 * Ensures: (scommand_length(self)==0) == scommand_is_empty()
 *
 */
    unsigned int length = 0;

    assert (self != NULL);

    length = g_queue_get_length (self->cmd);

    assert ((length == 0) == scommand_is_empty (self));

    return length;
}

const_bstring scommand_front (const scommand *self) {
/*
 * Toma la cadena de adelante de la secuencia de cadenas.
 * self: comando simple al cual tomarle la cadena del frente.
 * Returns: cadena del frente. La cadena retornada sigue siendo propiedad
 * del TAD, y debería considerarse inválida si luego se llaman a
 * modificadores del TAD. Hacer una copia si se necesita una cadena propia.
 * Requires: self!=NULL && !scommand_is_empty(self)
 * Ensures: result!=NULL
 */
    const_bstring front = NULL;

    assert (self != NULL);
    assert (!scommand_is_empty (self));

    front = g_queue_peek_head (self->cmd);

    assert (front != NULL);

    return front;
}

const_bstring scommand_get_redir_out (const scommand *self) {
/*
 * Obtiene los nombres de archivos a donde redirigir la entrada (salida).
 * self: comando simple a decidir si está vacío.
 * Returns: nombre del archivo a donde redirigir la entrada (salida)
 * o NULL si no está redirigida.
 * Requires: self!=NULL
 */
    bstring get_out = NULL;

    assert (self != NULL);

    get_out = self->out;

    return get_out;
}

const_bstring scommand_get_redir_in (const scommand *self) {
/*
 * Obtiene los nombres de archivos a donde redirigir la entrada (salida).
 * self: comando simple a decidir si está vacío.
 * Returns: nombre del archivo a donde redirigir la entrada (salida)
 * o NULL si no está redirigida.
 * Requires: self!=NULL
 */
    bstring get_in = NULL;

    assert (self != NULL);

    get_in = self->in;

    return get_in;
}

bstring scommand_to_string (const scommand *self) {
/* Preety printer para hacer debugging/logging.
 * Genera una representación del comando simple en un string (aka "serializar")
 * self: comando simple a convertir.
 * Returns: un string con la representación del comando simple similar
 * a lo que se escribe en un shell. El llamador es dueño del string
 * resultante.
 * Requires: self!=NULL
 * Ensures: scommand_is_empty(self) ||
 * scommand_get_redir_in(self)==NULL || scommand_get_redir_out(self)==NULL ||
 * blength(result)>0
 */
   GQueue *queue = NULL;
    bstring result = NULL;
    bstring current = NULL;

    assert (self != NULL);

    if (!g_queue_is_empty (self->cmd)) {
        queue = g_queue_copy (self->cmd);

        while (!g_queue_is_empty (queue)) {
            current = bstrcpy (g_queue_peek_head (queue));
            if (result == NULL) {
                result = current;
            } else {
                bconchar (result, WHITE_SPACE_CHAR);
                bconcat (result, current);
                if (current != NULL) {
                    bdestroy (current);
                }
            }
            g_queue_pop_head (queue);
        }
    }

    if (self->out != NULL) {
        current = bstrcpy (self->out);
        bconchar (result, WHITE_SPACE_CHAR);
        bconchar (result, REDIR_OUT_CHAR);
        bconchar (result, WHITE_SPACE_CHAR);
        bconcat (result, current);
        if (current != NULL) {
            bdestroy (current);
        }
    }

    if (self->in != NULL) {
        current = bstrcpy (self->in);
        bconchar (result, WHITE_SPACE_CHAR);
        bconchar (result, REDIR_IN_CHAR);
        bconchar (result, WHITE_SPACE_CHAR);
        bconcat (result, current);
        if (current != NULL) {
            bdestroy (current);
        }
    }

    assert (scommand_is_empty (self) || scommand_get_redir_in (self) == NULL
    || scommand_get_redir_out (self) == NULL ||  blength (result) > 0);

    return result;
}


/*
 * pipeline: tubería de comandos.
 * Ejemplo: ls -l *.c > out < in  |  wc  |  grep -i glibc  &
 * Secuencia de comandos simples que se ejecutarán en un pipeline,
 * más un booleano que indica si hay que esperar o continuar.
 *
 * Una vez que un comando entra en el pipeline, la memoria pasa a ser propiedad
 * del TAD. El llamador no debe intentar liberar la memoria de los comandos que
 * insertó, ni de los comandos devueltos por pipeline_front().
 * pipeline_to_string() pide memoria internamente y debe ser liberada
 * externamente.
 *
 * Externamente se presenta como una secuencia de comandos simples donde:
 *           ______________________________
 *  front -> | scmd1 | scmd2 | ... | scmdn | <-back
 *           ------------------------------
 */


/* Pipeline private structure */
struct pipeline_s {
    GQueue *cmd;
    bool wait;
};


pipeline *pipeline_new (void) {
/*
 * Nuevo `pipeline', sin comandos simples y establecido para que espere.
 * Returns: nuevo pipeline sin comandos simples y que espera.
 * Ensures: result != NULL
 * && pipeline_is_empty(result)
 * && pipeline_get_wait(result)
 */
    pipeline *new_pipeline = NULL;

    new_pipeline = calloc (1, sizeof (*new_pipeline));
    new_pipeline->cmd = g_queue_new ();
    new_pipeline->wait = true;

    assert (new_pipeline != NULL);
    assert (pipeline_is_empty (new_pipeline));
    assert (pipeline_get_wait (new_pipeline));

    return new_pipeline;
}

void pipeline_destroy (pipeline *self) {
/*
 * Destruye `self'.
 * self: tubería a a destruir.
 * Requires: self != NULL
 */
    assert (self != NULL);
    if (self->cmd != NULL)
        g_queue_free_full (self->cmd, (GDestroyNotify) scommand_destroy);
    self->cmd = NULL;
    self->wait = true;

    free (self);
    self = NULL;
}

/* Modificadores */

void pipeline_push_back (pipeline *self, scommand *sc) {
/*
 * Agrega por detrás un comando simple a la secuencia.
 * self: pipeline al cual agregarle el comando simple.
 * sc: comando simple a agregar. El TAD se apropia del comando.
 * Requires: self!=NULL && sc!=NULL
 * Ensures: !pipeline_is_empty()
 */
    assert (self != NULL);
    assert (sc != NULL);

    g_queue_push_tail (self->cmd, sc);

    assert (!pipeline_is_empty (self));
}

void pipeline_pop_front (pipeline *self) {
/*
 * Quita el comando simple de adelante de la secuencia.
 * self: pipeline al cual sacarle el comando simple del frente.
 * Destruye el comando extraido.
 * Requires: self!=NULL && !pipeline_is_empty(self)
 */
    gpointer pop_front = NULL;

    assert (self != NULL && !pipeline_is_empty (self));

    scommand_destroy (g_queue_peek_head (self->cmd));
    pop_front = g_queue_pop_head (self->cmd);
}

void pipeline_set_wait (pipeline *self, const bool w) {
/*
 * Define si el pipeline tiene que esperar o no.
 * self: pipeline que quiere ser establecido en su atributo de espera.
 * Requires: self!=NULL
 */
    assert (self != NULL);

    self->wait = w;
}

/* Proyectores */

bool pipeline_is_empty (const pipeline *self) {
/*
 * Indica si la secuencia de comandos simples tiene longitud 0.
 * self: pipeline a decidir si está vacío.
 * Returns: ¿Está vacío de comandos simples el pipeline?
 * Requires: self!=NULL
 */
    bool empty_pipeline = true;

    assert (self != NULL);

    empty_pipeline = g_queue_is_empty (self->cmd);

    return empty_pipeline;
}

unsigned int pipeline_length (const pipeline *self) {
/*
 * Da la longitud de la secuencia de comandos simples.
 * self: pipeline a medir.
 * Returns: largo del pipeline.
 * Requires: self!=NULL
 * Ensures: (pipeline_length(self)==0) == pipeline_is_empty()
 *
 */
    unsigned int length = 0;

    assert (self!=NULL);

    length = g_queue_get_length (self->cmd);

    assert ((length == 0) == pipeline_is_empty (self));

    return length;
}

scommand *pipeline_front (const pipeline *self) {
/*
 * Devuelve el comando simple de adelante de la secuencia.
 * self: pipeline al cual consultar cual es el comando simple del frente.
 * Returns: comando simple del frente. El comando devuelto sigue siendo
 * propiedad del TAD.
 * El resultado no es un "const scommand *" ya que el llamador puede
 * hacer modificaciones en el comando, siempre y cuando no lo destruya.
 * Requires: self!=NULL && !pipeline_is_empty(self)
 * Ensures: result!=NULL
 */
    scommand *front = NULL;

    assert (self != NULL && !pipeline_is_empty (self));

    front = g_queue_peek_head (self->cmd);

    assert (front != NULL);

    return front;
}

bool pipeline_get_wait (const pipeline *self) {
/*
 * Consulta si el pipeline tiene que esperar o no.
 * self: pipeline a decidir si hay que esperar.
 * Returns: ¿Hay que esperar en el pipeline self?
 * Requires: self!=NULL
 */

    assert (self != NULL);

    return self->wait;
}

bstring pipeline_to_string (const pipeline *self) {
/* Pretty printer para hacer debugging/logging.
 * Genera una representación del pipeline en una cadena (aka "serializar").
 *   self: pipeline a convertir.
 *   Returns: una cadena con la representación del pipeline similar
 *     a lo que se escribe en un shell. Debe destruirla el llamador.
 * Requires: self!=NULL
 * Ensures: pipeline_is_empty(self) || pipeline_get_wait(self) ||
 *              blength(result)>0
 */
    GQueue *queue = NULL;
    bstring result = NULL;
    scommand *current;

    assert (self != NULL);

    if (!g_queue_is_empty (self->cmd)) {
        queue = g_queue_copy (self->cmd);
        while (!g_queue_is_empty (queue)) {
            current = g_queue_peek_head (queue);
            if (result == NULL) {
                result = scommand_to_string (current);
            }
            else {
                bconchar (result, WHITE_SPACE_CHAR);
                bconchar (result, PIPE_CHAR);
                bconchar (result, WHITE_SPACE_CHAR);
                bconcat (result, scommand_to_string (current));
            }
            g_queue_pop_head (queue);
        }
    }

    if (!self->wait) {
        bconchar (result, WHITE_SPACE_CHAR);
        bconchar (result, WAIT_SYMBOL);
    }

    assert(pipeline_is_empty (self) || pipeline_get_wait (self) ||
            blength (result) > 0);

    return result;
}
