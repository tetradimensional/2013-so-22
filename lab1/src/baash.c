#define _BSD_SOURCE

#include <assert.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include "builtin.h"
#include "execute.h"
#include "parser.h"
#include "sys/wait.h"


static void print_prompt (void) {
    char *uname = NULL;
    char *hname = NULL;
    char *workdir = NULL;

    hname = calloc (20, sizeof (char));
    workdir = calloc (100, sizeof (char));
    uname = getenv ("USER");
    gethostname (hname, 20);
    getcwd (workdir, 100);
    printf ("\033[00;36m%s@%s:%s$ \033[00m", uname, hname, workdir);

    free (hname);
    hname = NULL;
    free (workdir);
    workdir = NULL;
}

static void print_welcome (char *file) {
    char c = '\0';
    FILE *fp = NULL;
    assert (file != NULL);

    fp = fopen (file, "r");
    if (fp == NULL) {
        perror ("baash: welcome file");
    } else {
        while ((c = getc (fp)) != EOF) {
            putchar (c);
        }
        fclose (fp);
    }
}


int main (void) {
    Parser *parser = NULL;
    pipeline *pipe = NULL;
    bool exit = false;
    int cstatus = 0;

    print_welcome ("welcome");
    parser = parser_new (stdin);
    while (!exit) {
        print_prompt ();
        pipe = parse_pipeline (parser);
        if (pipe != NULL) {
            if (is_exit (pipeline_front (pipe))) {
                exit = true;
            }
            else {
                execute_pipeline (pipe);
            }
            pipeline_destroy (pipe);
        }
        /* kill remaining zombies! (if any) */
        while (waitpid (-1, &cstatus, WNOHANG) > 0);
    }

    parser_destroy (parser);
    return 0;
}
