ChiquitinFS: Informe
==================

> _Grupo 22:_ Ledesma Noelia, Mancera Rodrigo, Godoy Lucas  
> Sistemas Operativos 2013  
> FaMAF - Univ. Nacional de Cordoba - Argentina  

Introduccion
------------

> En este proyecto seguimos lo especificado en el enunciado para realizar un
> sistema de archivos sencillo. El diseño del mismo fue realizado por la
> catedra y nuestro trabajo, en particular, fue implementar la siguientes
> operaciones: `statfs, getattr, chmod, chown, utimens, rename, readdir, read`.


Herramientas usadas
-------------------

> El proyecto fue implementado en C como lo especificado y gracias a FUSE
> nos evitamos tener que lidear con los problemas relacionados a desarrollar en
> espacio de kernel y, en vez de eso, trabajar comodos en espacio de usuario.
> Ademas de lo mencionado se utilizaron una bateria de tests comprobar la
> correctitud del sistema de archivos y una serie de ejemplos para las pruebas
> el sistema sobre los mismos, ambos implementados por la catedra.

Aspectos de la implementacion
-----------------------------

> ### Funciones Auxiliares
>> Fue necesario (y de gran ayuda) la creacion de las siguientes funciones
>> auxiliares:  
  
>> *   `find_path` Es la funcion que se encarga de encontrar la entrada en
>>     el directorio raiz de un archivo.  
>>     Fue muy importante el uso de aritmetica de punteros y de casteo para
>>     realizar bien la funcion.

>> *   `get_available_blocks` Necesaria para implementar `chiquitin_statfs`.

>> *   `get_file_block` Funcion que encuentra el i-esimo bloque de un
>>     archivo. Util para moverse dentro de un archivo y para implementar
>>     la operacion `chiquitin_read`


> ### Programacion Defensiva
>> Se trato de aplicarla lo mas posible a lo largo de todo el laboratorio.
>> En particular funciones como `chiquitin_create, chiquitin_destroy` muestran
>> su uso. Fue muy importante tener en cuenta de liberar los recursos
>> utilizados al momento de una falla o error de manera de salir "gracefully".
>> En la funcion `chiquitin_create` fue complicado lograr el comportamiento
>> adecuado de la funcion con respecto a fallos.


Problemas encontrados
---------------------

+ Tuvimos dificultades para lograr el correcto funcionamiento de las funciones:
  `read, create, statfs y getattr`. Pero a base de prueba y error, y varias
  releidas del enunciado pudimos implementarlas.

+ Intentamos probar todo el proyecto antes de que la operacion `read` estuviera
  finalizada, lo cual nos llevo a varios dolores de cabeza y a funcionamientos
  inesperados con los ejemplos provistos.

+ Nos costo comprender todo el enunciado y decidir como dividir la tarea a
  realizar.

+ A la hora de leer los tests para entender porque estaban fallando tuvimos que
  aprender un poco the bash scripting.
