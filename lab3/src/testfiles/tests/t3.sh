# DISK_IMAGE: sc_test3.chfs.gz

# En nuestros test podemos definir nuestras propias variables o podemos usar
# como sólo lectura las varibles:
#   MOUNT_POINT = punto de montaje del disco
#   STANDARD_DISK_NAME = nombre del disco estándar para cargar
#   TEST_FOLDER = carpeta donde estan los tests
#   TEST_DATA_FOLDER = carpeta donde los tests almacenan info

# También podmos usar la función:
#    fail $ERROR: para salir con el error $ERROR de un test



ALU_MD5="65ce0f05aff56277bb2d939a06e493a0  -"
FRIDA_MD5="363af4b719ae0d89e9ba0f0002dabae8  -"

BEGIN_TEST frida_file_reads_ok {
    MD5=$(cat $MOUNT_POINT/3.jpg | md5sum)
    if [[ "$MD5" != "$FRIDA_MD5" ]]; then
        fail "3.jpg md5sum is incorrect, read must be failing."
    fi
}

BEGIN_TEST alu_file_reads_ok {
    MD5=$(cat $MOUNT_POINT/wall_alu.jpg | md5sum)
    if [[ "$MD5" != "$ALU_MD5" ]]; then
        fail "wall_alu.jpg md5sum is incorrect, read must be failing."
    fi
}

BEGIN_TEST read_diferentes_offsets {
    python "$TEST_DATA_FOLDER/read_diferentes_offsets.py" \
           "$MOUNT_POINT/wall_alu.jpg"
}
