# DISK_IMAGE: sc_test2.chfs.gz

# En nuestros test podemos definir nuestras propias variables o podemos usar
# como sólo lectura las varibles:
#   MOUNT_POINT = punto de montaje del disco
#   STANDARD_DISK_NAME = nombre del disco estándar para cargar
#   TEST_FOLDER = carpeta donde estan los tests
#   TEST_DATA_FOLDER = carpeta donde los tests almacenan info

# También podmos usar la función:
#    fail $ERROR: para salir con el error $ERROR de un test

TEST_PREFIX="test"
TIMES=9
EXE="holamundo.sh"

BEGIN_TEST stat_archivos_no_nulos {
    i=1
    while [ "$i" -lt "$TIMES" ]
    do
	    ls "$MOUNT_POINT/$TEST_PREFIX$i.test" > /dev/null
	    let i=i+1
    done
}

BEGIN_TEST read_archivos_diferentes_tamanos {
    i=1
    while [ "$i" -lt "$TIMES" ]
    do
	    local LOCAL_FILE_="$MOUNT_POINT/$TEST_PREFIX$i.test"
	    local ORIG_FILE_="$TEST_DATA_FOLDER/$TEST_PREFIX$i.test" 
	    if [[ $(diff "$LOCAL_FILE_" "$ORIG_FILE_") ]]; then
            fail "$LOCAL_FILE_ no es igual a $ORIG_FILE_"
        fi
	    let i=i+1
    done
}

BEGIN_TEST read_diferentes_offsets {
    i=5
    python "$TEST_DATA_FOLDER/read_diferentes_offsets.py" "$MOUNT_POINT/$TEST_PREFIX$i.test"
}

BEGIN_TEST execute_command {
    if [[ $("$MOUNT_POINT/$EXE") != "hola" ]]; then
        fail "Problemas al ejecutar $MOUNT_POINT/$EXE"
    fi
}
