# DISK_IMAGE: sc_test1.chfs.gz

# En nuestros test podemos definir nuestras propias variables o podemos usar
# como sólo lectura las varibles:
#   MOUNT_POINT = punto de montaje del disco
#   STANDARD_DISK_NAME = nombre del disco estándar para cargar
#   TEST_FOLDER = carpeta donde estan los tests
#   TEST_DATA_FOLDER = carpeta donde los tests almacenan info

# También podmos usar la función:
#    fail $ERROR: para salir con el error $ERROR de un test


TEST_FILE="$MOUNT_POINT/test.test"
TEST_PREFIX="$MOUNT_POINT/tf"
TIMES=25
MOVIE_NAME="dexter.mkv"
MOVIE="$MOUNT_POINT/$MOVIE_NAME"
MOVIE_ORIGINAL="$TEST_DATA_FOLDER/$MOVIE_NAME"

BEGIN_TEST stat_archivo {
    # $TEST_FILE está"
    if [[ ! -a $TEST_FILE ]]; then
        fail "$TEST_FILE no se puede encontrar"
    fi
}

BEGIN_TEST renombre_basico {
    # Cambiamos el nombre de $TEST_FILE a $TEST_FILE.tmp
    mv $TEST_FILE $TEST_FILE.tmp 
    # $TEST_FILE no está
    if [[ -a $TEST_FILE ]]; then
        fail "El archivo no debería estar"
    fi
    # Restauramos el nombre de $TEST_FILE
    mv $TEST_FILE.tmp $TEST_FILE
}

BEGIN_TEST stat_varios_archivos {
    # Ver que hay archivos [0,$TIMES)
    i=0
    while [ "$i" -lt "$TIMES" ]
    do
	    ls "$TEST_PREFIX$i.test" > /dev/null
	    let i=i+1
    done
}

BEGIN_TEST consistencia_read {
    # Movie existe
    if [[ ! -a  $MOVIE ]]; then
        fail "No se puede leer $MOVIE"
    fi
    # Comparar $MOVIE del disco montado con la versión original"
    if [[ $(diff $MOVIE $MOVIE_ORIGINAL) ]]; then
        fail "$MOVIE no es igual a $MOVIE_ORIGINAL"
    fi
}

BEGIN_TEST renombre_consistencia {
    # Renombrando el archivo de video
    mv $MOVIE ${MOVIE}moved
    # Comparando que la copia tenga la misma informacion que la original
    diff $MOVIE_ORIGINAL ${MOVIE}moved
    # "Volvemos $MOVIE a su nombre original"
    mv ${MOVIE}moved $MOVIE
}

BEGIN_TEST dueño_de_archivos {
    # Le cambiamos el dueño a $TEST_FILE (a root)
    chown root:root $TEST_FILE
    uid=`stat -c "%u" $TEST_FILE`
    gid=`stat -c "%g" $TEST_FILE`
    if [ "$uid" -ne 0 ] || [ "$gid" -ne 0 ]; then
	    fail "El dueño no es root!"
    fi
    # El dueño de $TEST_FILE no es el uid/gid
    uid=`stat -c "%u" $TEST_FILE`
    gid=`stat -c "%g" $TEST_FILE`
    if [ "$uid" == "$UID" ] || [ "$gid" == "$GROUPS" ]; then
	    fail "El dueño no debería ser yo"
    fi
    # El dueño de $TEST_FILE ahora si es el actual usuario
    chown $UID:$GROUPS $TEST_FILE
    uid=`stat -c "%u" $TEST_FILE`
    gid=`stat -c "%g" $TEST_FILE`
    if [ "$uid" != "$UID" ] || [ "$gid" != "$GROUPS" ]; then
	    fail "El dueño debería ser yo"
    fi
}

BEGIN_TEST tiempos_de_acceso_no_nulos {
    echo -n "  Los tiempos no son 0"
    if [ `stat -c "%X" $TEST_FILE` = 0 ] || \
       [ `stat -c "%Y" $TEST_FILE` = 0 ] || \
       [ `stat -c "%Z" $TEST_FILE` = 0 ]; then
    	fail "Los tiempos de acceso no deberían ser nulos"
    fi
}

BEGIN_TEST read_modifica_metadatos {
    # Si leo $TEST_FILE el tiempo de Acceso avanza
    #falsos negativos con mount -o noatime
    atime=`stat -c "%X" $TEST_FILE`
    sleep 1
    cat $TEST_FILE > /dev/null
    if [ `stat -c "%X" $TEST_FILE` -le "$atime" ]; then
	    fail "No se modificó el tiempo de acceso"
    fi
}

BEGIN_TEST chmod_modifica_metadatos {
    # Hago chmod de $TEST_FILE y el tiempo de Cambio avanza
    ctime=`stat -c "%Z" $TEST_FILE`
    sleep 1
    chmod 777 $TEST_FILE
    if [ `stat -c "%Z" $TEST_FILE` -le "$ctime" ]; then
    	fail "No se modificó el tiempo de cambio"
    fi
    chmod 644 $TEST_FILE
}

BEGIN_TEST chow_modifica_metadatos {
    # Hago chown de $TEST_FILE y el tiempo de Cambio avanza
    ctime=`stat -c "%Z" $TEST_FILE`
    sleep 1
    #supongo que estas variables de entorno están bien definidas
    chown $UID:$GROUPS $TEST_FILE
    if [ `stat -c "%Z" $TEST_FILE` -le "$ctime" ]; then
    	die
    fi
}
