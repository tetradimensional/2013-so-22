#!/bin/bash
if [ -z "$1" ]; then
	echo "uso: $0 mi.chfs.gz"
	exit
fi
fusermount -u mnt
rm -f disk.chfs
cp $1 disk.chfs.gz
gunzip disk.chfs.gz
