#include "chiquitin.h"
#include <assert.h>
#include <libgen.h>
#include <errno.h>
#include <malloc.h>
#include <stdbool.h>
#include <stdio.h>
#include <string.h>
#include <sys/mman.h>
#include <time.h> /* time_t */
#include <unistd.h>

/* Invariante de representación */
#define INVREP(self) ((self)!=NULL && (self)->disk_mem!=NULL && 0<(self)->disk_fd)

/* Parámetros del disco */
#define BLOCKS 65536
#define BLOCK_SIZE 512
#define CHFS_FILENAME "disk.chfs"

/* Parámetros de la organización lógica del disco */
/* superblock */
#define CHFS_SUPER_MAGIC (0xC4F5)
#define SUPERBLOCK_SIZE (1*BLOCK_SIZE)
/* free block bitmap */
#define FBB_SIZE (16*BLOCK_SIZE)
/* root dir */
#define ROOTDIR_ENTRIES 128
#define DIR_ENTRY_SIZE 64
#define ROOTDIR_SIZE (ROOTDIR_ENTRIES*DIR_ENTRY_SIZE)
#define NAME_LENGTH 20
#define DIRECT_BLOCKS 10
/* block pool */
#define DIRECT_BLOCKS_POOL 255

/* Posiciones absolutas de cada una de las secciones del disco */
#define SUPERBLOCK_OFFSET 0
#define FBB_OFFSET (SUPERBLOCK_OFFSET+SUPERBLOCK_SIZE)
#define ROOTDIR_OFFSET (FBB_OFFSET+FBB_SIZE)
#define BLOCKPOOL_OFFSET (ROOTDIR_OFFSET+ROOTDIR_SIZE)

#define CHFS_SIZE (BLOCKS*BLOCK_SIZE)
#define BLOCKPOOL_SIZE ((CHFS_SIZE)-(SUPERBLOCK_SIZE+FBB_SIZE+ROOTDIR_SIZE))
#define BLOCKPOOL_BLOCKS (BLOCKPOOL_SIZE/BLOCK_SIZE)

#define ROOTDIR "/"

/* Operaciones FBB */
#define SET_BIT_0(buf, i) ((buf)[(i)/8]&=~(1u<<(i)%8))
#define SET_BIT_1(buf, i) ((buf)[(i)/8]|=1<<(i)%8)
#define GET_BIT(buf, i) ((buf)[(i)/8]>>(i)%8&1)

/* Operaciones varias */
#define MAX(a,b) (((a)>(b))?(a):(b))
#define MIN(a,b) (((a)<(b))?(a):(b))
#define DIV_CEIL(a,b) (((a)+(b)-1)/(b))

/* No quiero huecos al medio */
#define PACKED __attribute__((packed))


/* El FS chiquitin es el archivo con el disco mapeado en memoria y su fd */
struct chiquitin_s
{
    char *disk_mem; /* puntero al inicio del disco mapeado en memoria */
    int disk_fd; /* fd del openfile con el disco */
    time_t mount_time; /* tiempo de montaje: a,m,ctime del rootdir */
};

/* Nos facilita el manejo del Bloque RootDir */
struct rootdir_entry_s {
    char filename [NAME_LENGTH];           /* Filename del fichero */
    uint32_t size;                         /* Tamaño del fichero */
    uint16_t uid;                          /* Número de usuario */
    uint16_t gid;                          /* Número de grupo */
    uint16_t perm;                         /* Atributos de acceso */
    uint32_t atime;                        /* Atributos de tiempo */
    uint32_t mtime;                        /* Atributos de tiempo */
    uint32_t ctime;                        /* Atributos de tiempo */
    uint16_t direct_blocks[DIRECT_BLOCKS]; /* Bloques directos */
    uint16_t indirect_block;               /* Bloques indirectos */
} PACKED;                                  /* Evita el padding en las estructuras */

/* Manejo del SuperBlock */
struct superblock_entry_s {
    uint16_t magic_number;
    uint32_t disk_size;
    uint32_t free_space;
} PACKED;

/* Estructura Blockpool. Necesaria para el manejo de bloques indirectos */
struct blockpool_entry_s {
    uint16_t direct_blocks[DIRECT_BLOCKS_POOL];
    uint16_t indirect_block_next;
} PACKED;


/*
 * Funciones auxiliares
 */

static rootdir_entry *find_path(chiquitin *self, const char *path) {
/*
 * Dado un `path` valido de archivo, devuelve un puntero a la posicion
 * de sus correspondientes metadatos en el rootdir. De no existir el
 * archivo devuelve NULL.
*/
    int i = 0;
    char *bname = NULL;
    char *pathcpy = NULL;
    rootdir_entry *result = NULL;
    rootdir_entry *inode = NULL;
    assert(path!=NULL);
    assert(INVREP(self));

    pathcpy = strdup(path);
    bname = basename(pathcpy);
    inode = (rootdir_entry *) (self->disk_mem + ROOTDIR_OFFSET);
    for (i = 0; i < ROOTDIR_ENTRIES && result == NULL; i++) {
        if (strcmp(inode[i].filename, bname) == 0) {
            result = inode + i;
        }
    }

    free(pathcpy);
    bname = NULL;
    return result;
}

static uint32_t get_available_blocks(chiquitin *self) {
/*
 * Devuelve la cantidad de bloques libres en `self`.
 */
    int i = 0;
    int result = 0;
    char *buf = NULL;

    buf = self->disk_mem + FBB_OFFSET;
    for (i = 0; i < BLOCKPOOL_BLOCKS; i++) {
        if (!GET_BIT(buf, i)){
            result++;
        }
    }

    return result;
}

static char *get_file_block(chiquitin *self, rootdir_entry *entry, unsigned int i) {
/*
 * get_file_block encuentra el i-esimo bloque de un archivo dado. Función
 * necesaria para chiquitin_read.
 */
    int j = 0;
    char *result = NULL;
    blockpool_entry *block = NULL;
    assert(INVREP(self));
    assert(entry!=NULL);

    if (DIRECT_BLOCKS > i) {
        result = self->disk_mem + BLOCKPOOL_OFFSET + entry->direct_blocks[i]
        * BLOCK_SIZE;
    } else if (i >= DIRECT_BLOCKS) {
        block = (blockpool_entry *)(self->disk_mem + BLOCKPOOL_OFFSET +
        entry->indirect_block * BLOCK_SIZE);
        for (j = 0; j < ((i - DIRECT_BLOCKS) / DIRECT_BLOCKS_POOL); j++) {
            block = (blockpool_entry *)(self->disk_mem + BLOCKPOOL_OFFSET +
            block->indirect_block_next * BLOCK_SIZE);
        }
        result = self -> disk_mem + BLOCKPOOL_OFFSET +
        block->direct_blocks[(i - DIRECT_BLOCKS) % DIRECT_BLOCKS_POOL] * BLOCK_SIZE;
    }

    return result;
}


/*
 * Constructores y Destructores
 */

chiquitin *chiquitin_create(void)
{
    int fd = 0;
    char *mem = NULL;
    time_t t = 0;
    chiquitin *result = NULL;

    result = calloc(1, sizeof(*result));
    if (result == NULL) {
        printf("chfs: couldn't allocate memory for chfs");
    } else {
        fd = open(CHFS_FILENAME, O_RDWR);
        if (fd == -1) {
            perror("chfs: open");
            free(result);
            result = NULL;
        } else {
            mem = mmap(NULL, BLOCKS*BLOCK_SIZE, PROT_READ|PROT_WRITE, MAP_SHARED, fd, 0);
            if (mem == ((void *) -1)) {
                perror("chfs: mmap");
                free(result);
                result = NULL;
            } else if (((superblock_entry *) mem)->magic_number != CHFS_SUPER_MAGIC) {
                printf("chfs: the image is not a valid chfs!");
                free(result);
                result = NULL;
            } else {
                result->disk_mem = mem;
                result->disk_fd = fd;
                t = time(NULL);
                if (t == -1) {
                    perror("chfs: time");
                } else {
                    result->mount_time = t;
                }
            }
        }
    }

    assert(result != NULL);
    assert(INVREP(result));
    return result;
}

void chiquitin_destroy(chiquitin *self)
{
    assert(INVREP(self));

    if (msync(self->disk_mem, BLOCKS*BLOCK_SIZE, MS_SYNC) == -1) {
        perror("chfs: msync");
    }
    if (munmap(self->disk_mem, BLOCKS*BLOCK_SIZE) == -1) {
        perror("chfs: munmap");
    }
    self->disk_mem = NULL;
    free(self); self = NULL;
}


/*
 * Operaciones Básicas
 */

int chiquitin_getattr(chiquitin *self, const char *path, struct stat *stbuf)
{
    int result = 0;
    rootdir_entry *entry = NULL;
    assert(INVREP(self));
    assert(path!=NULL); assert(stbuf!=NULL);

    memset(stbuf, 0, sizeof(struct stat));
    if (strcmp(path, ROOTDIR) == 0) {
        stbuf->st_mode = S_IFDIR | 0755;
        stbuf->st_atime = self->mount_time;
        stbuf->st_mtime = self->mount_time;
        stbuf->st_ctime = self->mount_time;
        stbuf->st_nlink = 2;
    } else {
        entry = find_path(self, path);
        if (entry != NULL) {  /* Si el path es un archivo dentro del rootdir */
            stbuf->st_size = entry->size;
            stbuf->st_uid = entry->uid;
            stbuf->st_gid = entry->gid;
            stbuf->st_mode = entry->perm;
            stbuf->st_atime = entry->atime;
            stbuf->st_mtime = entry->mtime;
            stbuf->st_ctime = entry->ctime;
            stbuf->st_nlink = 1;
        }
        else {
            errno = ENOENT;
            result = -ENOENT;
        }
    }

    return result;
}

int chiquitin_readlink(chiquitin *self, const char *path, char *buf, size_t size)
{
    int result = 0;
    assert(INVREP(self));
    assert(path!=NULL && buf!=NULL);

    result = 0;
    return result;
}

int chiquitin_mknod(chiquitin *self, const char *path, mode_t mode)
{
    int result = 0;
    assert(INVREP(self));
    assert(path!=NULL);

    result = 0;
    return result;
}

int chiquitin_unlink(chiquitin *self, const char *path)
{
    int result = 0;
    assert(INVREP(self));
    assert(path!=NULL);

    result = 0;
    return result;
}

int chiquitin_symlink(chiquitin *self, const char *from, const char *to)
{
    int result = 0;
    assert(INVREP(self));
    assert(from!=NULL && to!=NULL);

    result = 0;
    return result;return -ENOENT;
}

int chiquitin_rename(chiquitin *self, const char *from, const char *to)
{
    int result = 0;
    char *to_cpy = NULL;
    char *to_name = NULL;
    rootdir_entry *entry = NULL;

    assert(INVREP(self));
    assert(from!=NULL && to!=NULL);

    to_cpy = strdup(to);
    to_name = basename(to_cpy);
    entry = find_path(self, from);
    if (strlen(to_name) <= NAME_LENGTH) {
        strncpy (entry -> filename, to_name, NAME_LENGTH);
        entry->ctime = time(NULL);
    } else {
        result = -ENAMETOOLONG;
    }

    free(to_cpy);
    to_name = NULL;
    return result;
}

int chiquitin_link(chiquitin *self, const char *from, const char *to)
{
    int result = 0;
    assert(INVREP(self));
    assert(from!=NULL && to!=NULL);

    result = 0;
    return result;
}

int chiquitin_chmod(chiquitin *self, const char *path, mode_t mode)
{
    int result = 0;
    time_t t = 0;
    rootdir_entry *inode = NULL;
    assert(INVREP(self));
    assert(path!=NULL);

    inode = find_path(self, path);
    if (inode == NULL) {
        errno = ENOENT;  /* the file does not exist */
        result = -1;
    } else {
        inode->perm = mode;
        t = time(NULL);
        if (t == -1) {
            perror("chfs: time");
        } else {
            inode->ctime = t;
        }
    }

    return result;
}

int chiquitin_chown(chiquitin *self, const char *path, uid_t uid, gid_t gid)
{
    int result = 0;
    time_t t = 0;
    rootdir_entry *inode = NULL;
    assert(INVREP(self));
    assert(path!=NULL);

    inode = find_path(self, path);
    if (inode == NULL) {
        errno = ENOENT;  /* the file does not exist */
        result = -1;
    } else {
        if (uid != -1) {
            inode->uid = uid;
        }
        if (gid != -1) {
            inode->gid = gid;
        }
        t = time(NULL);
        if (t == -1) {
            perror("chfs: time");
        } else {
            inode->ctime = t;
        }
    }

    return result;
}

int chiquitin_truncate(chiquitin *self, const char *path, off_t size)
{
    int result = 0;
    assert(INVREP(self));
    assert(path!=NULL);

    result = 0;
    return result;
}

int chiquitin_read(chiquitin *self, const char *path,
                   char *buffer, size_t size, off_t offset)
{
    int result = 0;
    char *auxb = NULL;
    size_t s = 0, e = 0;
    size_t sb = 0, eb = 0;
    size_t srb = 0, erb = 0;
    size_t bytes_read = 0;
    size_t bytes_must_read = 0;
    rootdir_entry *entry = NULL;

    assert(INVREP(self));
    assert(path!=NULL);
    assert(buffer!=NULL);

    entry = find_path(self, path);
    if (offset > entry->size) {
        s = entry -> size;
    } else {
        s = offset;
    }
    if (offset + size > entry -> size) {
        e = entry -> size;
    } else {
        e = offset + size;
    }

    bytes_must_read = e - s;
    assert(s<=e);

    sb = s/BLOCK_SIZE;
    eb = e/BLOCK_SIZE;
    srb = s%BLOCK_SIZE;
    erb = e%BLOCK_SIZE;

    memset(buffer + bytes_must_read, '\0', size - bytes_must_read);
    auxb = buffer;

    while (s < e) {
        if (sb != eb) {
            assert (s == sb * BLOCK_SIZE + srb);
            assert (e == eb * BLOCK_SIZE + erb);
            memcpy(auxb, get_file_block(self, entry, sb) + srb, BLOCK_SIZE - srb);
            auxb += BLOCK_SIZE - srb;
            s += BLOCK_SIZE - srb;
            bytes_read += BLOCK_SIZE - srb;
            srb = 0;
            ++sb;
        } else {
            memcpy(auxb, get_file_block(self, entry, sb) + srb, erb-srb);
            auxb += erb - srb;
            s += erb - srb;
            bytes_read += erb - srb;
            srb = erb;
        }
    }

    entry->atime = time(NULL);
    result = bytes_read;
    return result;
}

int chiquitin_write(chiquitin *self, const char *path,
                    const char *buffer, size_t size, off_t offset)
{
    int result = 0;
    assert(INVREP(self));
    assert(path!=NULL);
    assert(buffer!=NULL);

    result = 0;
    return result;
}

int chiquitin_statfs(chiquitin *self, const char *path, struct statvfs *stbuf) {
    int result = 0;
    superblock_entry *entry = NULL;
    assert(INVREP(self));
    assert(path!=NULL);
    assert(stbuf!=NULL);
    assert(self->disk_mem != NULL);

    memset(stbuf, 0, sizeof(struct statvfs));
    entry = (superblock_entry *) (self->disk_mem + SUPERBLOCK_OFFSET);
    stbuf->f_bsize = BLOCK_SIZE;
    stbuf->f_blocks = BLOCKPOOL_BLOCKS;
    stbuf->f_bfree = get_available_blocks(self);
    stbuf->f_bavail = get_available_blocks(self);
    stbuf->f_files = ROOTDIR_ENTRIES;
    stbuf->f_flag = ST_RDONLY;
    stbuf->f_namemax = NAME_LENGTH;

    result = 0;
    return result;
}

int chiquitin_utimens(chiquitin *self, const char *path, const struct timespec tv[2])
{
    int result = 0;
    struct timespec *ts = NULL;
    time_t t = 0;
    rootdir_entry *inode = NULL;
    assert(INVREP(self));
    assert(path!=NULL);

    inode = find_path(self, path);
    if (inode == NULL) {
        errno = ENOENT;
        result = -1;
    } else if (tv[0].tv_nsec == UTIME_OMIT && tv[0].tv_nsec == UTIME_OMIT) {
    } else {
        if (tv == NULL || (tv[0].tv_nsec == UTIME_NOW && tv[1].tv_nsec == UTIME_NOW)) {
            clock_gettime(CLOCK_REALTIME, ts);  /* set to current time */
            inode->atime = ts[0].tv_nsec;
            inode->mtime = ts[1].tv_nsec;
        } else {
            inode->atime = tv[0].tv_nsec;
            inode->mtime = tv[1].tv_nsec;
        }
        t = time(NULL);
        if (t == -1) {
            perror("chfs: time");
        } else {
            inode->ctime = t;
        }
    }

    return result;
}

/*
 * Operaciones sobre Directorios
 */

int chiquitin_mkdir(chiquitin *self, const char *path, mode_t mode)
{
    int result = 0;
    assert(INVREP(self));
    assert(path!=NULL);

    result = 0;
    return result;
}

int chiquitin_rmdir(chiquitin *self, const char *path)
{
    int result = 0;
    assert(INVREP(self));
    assert(path!=NULL);

    result = 0;
    return result;
}

int chiquitin_readdir(chiquitin *self, const char *path, void *buf,
                      fuse_fill_dir_t filler)
{
    int i = 0;
    int result = 0;
    rootdir_entry *entry = NULL;

    assert(INVREP(self));
    assert(path!=NULL && buf!=NULL);

    if (strcmp(path, ROOTDIR) == 0) {
        filler(buf, ".", NULL, 0);
        filler(buf, "..", NULL, 0);
        entry = (rootdir_entry *) (self->disk_mem + ROOTDIR_OFFSET);
        for (i = 0; i < ROOTDIR_ENTRIES; i++) {
            if (strlen(entry->filename) > 0) {
                filler(buf, entry->filename, NULL, 0);
            }
            entry++;
        }
    } else {
        return -ENOENT;
    }

    return result;
}
