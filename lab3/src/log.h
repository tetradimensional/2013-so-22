/* $Date: 2010-09-30 15:40:39 -0300 (Thu, 30 Sep 2010) $ */
/* $Revision: 1364 $ */

#ifndef __LOG_H
#define __LOG_H

#include <stdio.h>

int log_open(const char *filename, const int level);
void log_close(void);

void debug1(const char *format, ...) __attribute__((format(printf, 1, 2)));
void debug2(const char *format, ...) __attribute__((format(printf, 1, 2)));
void debug3(const char *format, ...) __attribute__((format(printf, 1, 2)));

#endif /* __LOG_H */ 
