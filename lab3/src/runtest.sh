#!/bin/bash

PROJECT_FOLDER=$(dirname "$0")
if [[ $PROJECT_FOLDER != "." ]]; then
    echo "Moviendome al directorio del proyecto"
    cd $PROJECT_FOLDER
fi

MOUNT_POINT="mnt"
STANDARD_DISK_NAME="disk.chfs"
TEST_FOLDER="testfiles/tests"
TEST_DATA_FOLDER="testfiles/data"
TEST_CASE_REGEX="^BEGIN_TEST[ \t]+(.*?)[ \t]*\{"


function fail()
{
    local ERROR_MESSAGGE_="$1"
    echo "$ERROR_MESSAGGE_" >&2
    return 1
}

function mount_()
{   # Monta una imagen de disco en $MOUNT_POINT
    # $1 = imagen a montar
    local DISK_IMAGE_=$1
    if [[ ! -d "$MOUNT_POINT" ]]; then
        mkdir -p "$MOUNT_POINT"
    fi
    if [[ -a "$STANDARD_DISK_NAME" ]]; then
        rm "$STANDARD_DISK_NAME"
    fi

    cp "$DISK_IMAGE_" "$STANDARD_DISK_NAME".gz
    gunzip "$STANDARD_DISK_NAME".gz
    
    echo "(Montando $PROJECT_FOLDER/$DISK_IMAGE_ en $PROJECT_FOLDER/$MOUNT_POINT)"
    ./fusewrapper "$MOUNT_POINT" 1,2> log.txt &
    
    sleep 2 # Por lo visto hace falta esperar un poco
}

function umount_()
{   # Desmonta el $MOUNT_POINT y limpia un poco (?
    echo "(Desmontando $PROJECT_FOLDER/$MOUNT_POINT)"
    fusermount -u "$MOUNT_POINT"
    rm disk.chfs
    rmdir "$MOUNT_POINT"
}

function translate_tcs()
{
    local TEST_=$1
    sed -re "s/^BEGIN_TEST[ \t]+(.+?)[ \t]*\{/function \1 \(\) \{/g" \
                "$TEST_FOLDER/$TEST_" 
}

function start_test()
{   # Inicia un test nuevo
    # $1 = nombre del test a iniciar
    # $2 = nombre de la imagen del disco a cargar
    local TEST_=$1
    local DISK_IMAGE_=$2
    local TEST_CASES_
    local ERROR_

    printf "\n\n"
    echo "#####################################################################"
    echo "# Comenzando test $PROJECT_FOLDER/$TEST_FOLDER/$TEST_ sobre"
    echo "#                 $PROJECT_FOLDER/$DISK_IMAGE_"
    echo "#####################################################################"
    
    load_test_cases "$TEST_" # llenar los nombres de los tc
    TEST_CASES_=( ${TEST_CASES[@]} )
    if [[ ! $TEST_CASES_ ]]; then
        echo "!!! Warning: No se encontraron tests"
    else
        for test_case in ${TEST_CASES_[@]}; do
            echo -n "  > Test $test_case"
            ERROR_=$($test_case 2>&1 > /dev/null)
            if [[ ! $ERROR_ ]]; then
                printf "\t[✔]\n"
            else
                ERROR_=$(printf "$ERROR_" | sed ':a;N;$!ba;s/\n/\n\t /g')
                printf "\t[✘]\n"
                printf "\terror:"; echo "$ERROR_"
            fi 
        done
    fi
}

function end_test()
{   # Terminamos de ejecutar un test
    # $1 = nombre del test a finalizar
    local TEST_=$1
    echo "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"
    printf "\n\n"
    rm "$TEST_FOLDER/$TEST_"~ 
}

function get_disk_image()
{   # Dado un test name, encuentra la imagen de disco a utilizar.
    # $1 = nombre del test a iniciar
    local TEST_=$1
    echo $(head "$TEST_FOLDER/$TEST_" -n 1 | \
           sed -re 's/^#[ \t]*DISK_IMAGE[ \t]*:[ \t]*(.*?)[ \t]*$/\1/p' | \
           head -n 1) 
}

function load_test_cases()
{   # LLena la varible de entorno TEST_CASES
    local TEST_=$1
    TEST_CASES=( $(sed -rn "s/^BEGIN_TEST[ \t]+(.*?)[ \t]*\{/\1/p" \
                 $TEST_FOLDER/$TEST_) )
}

function run_test()
{   # Ejecutar un archivo de test entero
    # $1 = nombre del test a iniciar
    local TEST_=$1
    local DISK_IMAGE_=$(get_disk_image "$TEST_")
    local TRANSLATION_
    if [[ ! $DISK_IMAGE_ ]]; then
        echo "   > $TEST_FOLDER/$TEST_ está mal formado. No se encontró imagen"
        echo "        a cargar"
    else
        mount_ "$DISK_IMAGE_"                 # montar disco
        cp "$TEST_FOLDER/$TEST_" "$TEST_FOLDER/$TEST_"~
        TRANSLATION_=$(translate_tcs "$TEST_") # traducimos a sc
        if [[ ! "$TRANSLATION_" ]]; then
            echo "!!! Warning: No se encontraron tests"
        else
            echo "$TRANSLATION_" > "$TEST_FOLDER/$TEST_"~
            source "$TEST_FOLDER/$TEST_"~          # cargamos los tc
            start_test "$TEST_" "$DISK_IMAGE_"     # corremos los tc
        fi
        end_test "$TEST_"
        umount_
    fi
}


function usage ()
{
    printf "Uso: $0\n"
    printf "\tEsta herramienta se encarga de correr todos los tests\n"
    printf "\tde este laboratorio. ($PROJECT_FOLDER/$TEST_FOLDER)\n"
}

# function Main ()

if [[ "$1" ]]; then
    usage
else
    echo "> Compilando código fuente ..."
    make 1> /dev/null || exit 1
    # Corremos todos los tests    
    for TEST_ in $(ls $TEST_FOLDER); do
        run_test "$TEST_"
    done
fi
