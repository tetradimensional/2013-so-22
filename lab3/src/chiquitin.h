#ifndef __CHIQUITIN_H
#define __CHIQUITIN_H

#define _GNU_SOURCE

#include <sys/types.h>
#include <sys/stat.h>
#include <sys/statvfs.h>
#include <utime.h>
#include <time.h>
#define FUSE_USE_VERSION 26 /* Se necesita por lo menos 2.6 */
#include <fuse.h> /* para fuse_fill_dir_t, off_t */

typedef struct chiquitin_s chiquitin;
typedef struct rootdir_entry_s rootdir_entry;
typedef struct superblock_entry_s superblock_entry;
typedef struct blockpool_entry_s blockpool_entry;

/* Constructores y Destructores */
chiquitin *chiquitin_create(void);
void chiquitin_destroy(chiquitin *self);

/* Operaciones Básicas */
int chiquitin_getattr(chiquitin *self, const char *path, struct stat *stbuf);
int chiquitin_readlink(chiquitin *self, const char *path, char *buf, size_t size);
int chiquitin_mknod(chiquitin *self, const char *path, mode_t mode);
int chiquitin_unlink(chiquitin *self, const char *path);
int chiquitin_symlink(chiquitin *self, const char *from, const char *to);
int chiquitin_rename(chiquitin *self, const char *from, const char *to);
int chiquitin_link(chiquitin *self, const char *from, const char *to);
int chiquitin_chmod(chiquitin *self, const char *path, mode_t mode);
int chiquitin_chown(chiquitin *self, const char *path, uid_t uid, gid_t gid);
int chiquitin_truncate(chiquitin *self, const char *path, off_t size);
int chiquitin_read(chiquitin *self, const char *path,
		char *buffer, size_t size, off_t offset);
int chiquitin_write(chiquitin *self, const char *path,
		 const char *buffer, size_t size, off_t offset);
int chiquitin_statfs(chiquitin *self, const char *path, struct statvfs *stbuf);
int chiquitin_utimens(chiquitin *self, const char *path, const struct timespec tv[2]);

/* Operaciones sobre Directorios */
int chiquitin_mkdir(chiquitin *self, const char *path, mode_t mode);
int chiquitin_rmdir(chiquitin *self, const char *path);
int chiquitin_readdir(chiquitin *self, const char *path, void *buf,
		   fuse_fill_dir_t filler);

#endif /* __CHIQUITIN_H */
