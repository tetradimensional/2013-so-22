#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/fs.h>
#include <linux/semaphore.h>

#define DEBUG
#include "debug.h"
#include "fifoops.h"
#include "rbuffer/rbuffer.h"

MODULE_AUTHOR("Mancera Rodrigo, Ledesma Noelia, Godoy Lucas");
MODULE_LICENSE("GPL");
MODULE_DESCRIPTION("Controlador para dispositivo /dev/fifo (buffer productor consumidor).");

const char *name = "fifo";
unsigned int size = 1024;
static int major = 248;
module_param(size, int, S_IRUGO);
module_param(major, int, S_IRUGO);

struct semaphore mutex;
struct semaphore fillcount;
struct semaphore emptycount;
rbuffer_t rbuffer = NULL;


int fifo_init(void)
{
    int error = 0;
    error = register_chrdev(major, name, &fifo_fops);
    if (error == 0) {
        dprintk("%s: Carga completada satisfactoriamente con major: %d.\n", name, major);
        rbuffer = rbuffer_create(size);
        if (rbuffer == NULL) {
            dprintk("%s: No se pudo crear el ring buffer.\n", name);
            unregister_chrdev(major, name);
            dprintk("%s: No se pudo registrar el dispositivo.\n", name);
            error = -1;
        }
        sema_init(&mutex, 1);
        sema_init(&fillcount, 0);
        sema_init(&emptycount, size);
    } else {
        dprintk("%s: No se pudo registrar el dispositivo '%s' con"
            " major: %d.\n", name, name, major);
        error = -1;
    }

    return error;
}

void fifo_exit(void)
{
    if (rbuffer != NULL) {
        rbuffer_destroy(rbuffer);
    }

    unregister_chrdev(major, name);
    dprintk("%s: Descarga completada satisfactoriamente.\n", name);
}

module_init(fifo_init);
module_exit(fifo_exit);
