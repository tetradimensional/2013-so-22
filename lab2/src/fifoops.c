#include <linux/kernel.h>
#include <linux/errno.h>
#include <linux/fs.h>
#include <linux/module.h>
#include <linux/uaccess.h>
#include <linux/semaphore.h>
#include <linux/slab.h>
#include <stdbool.h>

#define DEBUG

#include "debug.h"
#include "fifoops.h"
#include "rbuffer/rbuffer.h"

extern unsigned int size;
extern rbuffer_t rbuffer;
extern struct semaphore mutex;
extern struct semaphore fillcount;
extern struct semaphore emptycount;

static unsigned int writers_count = 0;
static unsigned int readers_count = 0;
static unsigned int read_count = 0;
static unsigned int rbuffer_size = 0;

static void print_log_info(const char* operation) {
    printk ("-----------------------------------------------\n");
    printk ("Operacion                          : %s  \n", operation);
    printk ("Cantidad de Lectores               : %d  \n", readers_count);
    printk ("Cantidad de Escritores             : %d  \n", writers_count);
    printk ("Cantidad de Elementos Leidos       : %d  \n", read_count);
    printk ("Cantidad de Elementos en el Buffer : %d  \n", rbuffer_size);
    printk ("-----------------------------------------------\n");
}

static int fifo_open(struct inode *ip, struct file *fp)
{
    /* Nos quedamos con sólo aquellos bits
     * que se refieren a los modos de acceso */
    unsigned int permission = fp->f_flags & O_ACCMODE;

    /* Comienzo de la sección crítica */
    if (down_interruptible (&mutex)) {
        return -ERESTARTSYS;
    }

    /* Se abrió para lectura y no hay lectores (permite a lo sumo un lector) */
    if (permission == O_RDONLY && readers_count == 0) {
        readers_count++;
        /* Si ingresa un nuevo lector, se reinicia la cuenta de
           bytes leídos */
        read_count = 0;
    /* Se abrió para escritura y no hay escritores (permite a lo sumo un escritor)
     * y no hay lectores ó hay un lector pero aún no se leyó nada */
    } else if (permission == O_WRONLY && writers_count == 0 && (readers_count == 0 || read_count == 0)) {
        writers_count++;
    } else {
        /* Los permisos no son válidos, se libera el mútex y se
           retorna error de acceso */
        /* Finalización de la sección crítica */
        up (&mutex);
        return -EACCES;
    }

    /* Finalizacion de la sección crítica */
    up (&mutex);
    print_log_info("Open");

    return 0;
}

static ssize_t fifo_read(struct file *fp, char *buf, size_t count, loff_t *offset)
{
    bool read_failed = false;
    unsigned long remaining = 0;
    unsigned int i = 0;
    byte *tmpbuf = NULL;

    /* Comienzo de sección crítica necesaria para establecer el tamaño del buffer */
    if (down_interruptible(&mutex)) {
        return -ERESTARTSYS;
    }

    /* Vemos de que tamaño es el rbuffer para saber al menos cuanto se va a leer */
    if (rbuffer_size == 0) {
        count = 1;
    } else if (rbuffer_size < count) {
        count = rbuffer_size;
    }
    
    /* Finalización de la sección crítica */
    up(&mutex);
    
    tmpbuf = kmalloc(count, GFP_KERNEL);
    if (tmpbuf == NULL) {
        dprintk("fifo: No se pudo crear tmpbuf temporal para lectura.\n");
        return -1;
    }

    for (i = 0; i < count; i++) {
        if (down_interruptible(&fillcount)) {
            kfree(tmpbuf);
            tmpbuf = NULL;
            dprintk("fifo: Error fatal cuando se estaba leyendo.\n");
            return -ERESTARTSYS;
        }
         /* Comienzo de la sección crítica */
        if (down_interruptible(&mutex)) {
            up(&fillcount);
            if (i == 0) {
                kfree(tmpbuf);
                tmpbuf = NULL;
                return -ERESTARTSYS;
            } else break;
        }

        read_failed = rbuffer_remove(rbuffer, tmpbuf + i);
        if (read_failed) {
            dprintk("fifo: rbuffer esta vacio.\n");
        } else {
            read_count++;
            rbuffer_size--;
            /* Finalización de la sección crítica */
            up(&emptycount);
        }
        up(&mutex);
    }

    remaining = copy_to_user(buf, tmpbuf, count);
    if (remaining != 0) {
        dprintk("fifo: Solo se pudieron leer %u bytes.\n",
                (unsigned int) (i-remaining));
    } else {
        dprintk("fifo: Se leyeron %u bytes satisfactoriamente.\n", i);
    }

    kfree (tmpbuf);
    tmpbuf = NULL;

    return i;
}

static ssize_t fifo_write(struct file *fp, const char *buf, size_t count, loff_t *offset)
{
    bool write_failed = false;
    unsigned long remaining = 0;
    unsigned int i = 0;
    ssize_t res = 0;
    byte *tmpbuf = NULL;

    /* Comienzo de sección crítica necesaria para establecer el tamaño del buffer */
    if (down_interruptible(&mutex)) {
        return -ERESTARTSYS;
    }
    /* Vemos de que tamaño es el rbuffer para saber al menos cuanto se va a escribir */
    if (size == rbuffer_size) {
        count = 1;
    } else if (size-rbuffer_size < count) {
        count = size-rbuffer_size;
    }
     /* Finalización de la sección crítica */
    up(&mutex);
    
    tmpbuf = kmalloc(count, GFP_KERNEL);
    if (tmpbuf == NULL) {
        dprintk("fifo: No se pudo crear buffer temporal para escritura.\n");
        return -1;
    }

    res = count;
    remaining = copy_from_user(tmpbuf, buf, count);
    if (remaining != 0) {
        dprintk("fifo: No se pudo copiar todo desde usuario."
                "Quedaron %ld bytes por copiar.\n", remaining);
        res -= remaining;
    }

    for (i = 0; i < res; i++) {
        if (down_interruptible(&emptycount)) {
            kfree(tmpbuf);
            tmpbuf = NULL;
            dprintk("fifo: Error fatal cuando se estaba escribiendo.\n");
            return -ERESTARTSYS;
        }
        /* Comienzo de la sección crítica */
        if (down_interruptible(&mutex)) {
            up(&emptycount);
            if (i == 0) {
                kfree(tmpbuf);
                tmpbuf = NULL;
                return -ERESTARTSYS;
            } else break;
        }

        write_failed = rbuffer_insert(rbuffer, tmpbuf[i]);
        if (write_failed) {
            dprintk("fifo: rbuffer esta lleno.\n");
        } else {
            rbuffer_size++;
            /* Finalización la sección crítica */
            up(&fillcount);
        }
        up(&mutex);
    }

    if (i != count) {
        dprintk("fifo: Solo se escribieron %u bytes.\n", i);
    } else {
        dprintk("fifo: Se escribieron %u bytes satisfactoriamente.\n", i);
    }

    kfree(tmpbuf);
    tmpbuf = NULL;

    return (ssize_t) i;
}

static int fifo_release(struct inode *ip, struct file *fp)
{
    /* Nos quedamos con sólo aquellos bits
     * que se refieren a los modos de acceso */
    unsigned int permission = fp->f_flags & O_ACCMODE;

    /* Comienzo de la sección crítica */
    if (down_interruptible (&mutex)) {
        return -ERESTARTSYS;
    }

    if (permission == O_RDONLY) {
        readers_count--;
    } else if (permission == O_WRONLY) {
        writers_count--;
    }

    /* Finalización de la sección crítica */
    up (&mutex);
    print_log_info("Release");

    return 0;
}

/**
 * La estructura que contiene punteros a cada una de las operaciones que se
 * pueden realizar sobre /dev/fifo. Esta es la única variable que exporta el
 * módulo. Así, podremos cambiar hasta los nombres de las funciones que
 * implementan cada operación y se preservará la abstracción.
 */

struct file_operations fifo_fops = {
    .owner  =   THIS_MODULE,
    .open   =   fifo_open,
    .read   =   fifo_read,
    .write  =   fifo_write,
    .release=   fifo_release
};
