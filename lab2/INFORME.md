/dev/fifo: Informe
==================

> _Grupo 22:_ Ledesma Noelia, Mancera Rodrigo, Godoy Lucas  
> Sistemas Operativos 2013  
> FaMAF - Univ. Nacional de Cordoba - Argentina  

Introduccion
------------

> En este proyecto seguimos lo especificado en el enunciado para realizar un
> controlador de dispositivos de caracteres en espacio de kernel.
> En particular el dispositivo es una cola que sirve como buffer entre dos
> procesos cualesquiera: un productor de datos y un consumidor.
> Es decir que en este laboratorio debemos tratar con dos problematicas en 
> particular, por un lado la de implementar un módulo de kernel y por el otro 
> tratar con el problema del productor consumidor.

Herramientas usadas
-------------------

> El proyecto fue implementado en C como lo especificado y se empleo la
> estructura rbuffer ya implementada y provista por la catedra.
> Dado que la implementacion de dicho modulo fue en espacio de kernel
> fue muy util tambien el archivo `debug.h` provisto para poder imprimir
> de manera sencilla en el kernel ring buffer y debugear.
> Por otro lado fue de mucha ayuda el documento `pcl.pdf` [0] provisto, el
> cual contiene mucha informacion para la correcta implementacion de
> modulos para el kernel de linux.

Aspectos de la implementacion
-----------------------------

> ### Operaciones

>> El modulo consiste de 2 archivos principales: un `main.c` que contiene las
>> variables globales y su inicializacion, y un `fifoops.c` que contiene las
>> operaciones que se pueden realizar en el dispositivo; en este caso leer
>> (`fifo_read`) y escribir (`fifo_write`). La funcion `fifo_read` crea un
>> buffer temporal donde luego se almacenan los contenidos solicitados del
>> rbuffer. Una vez lleno, se copia el buffer temporal al espacio de usuario
>> usando la funcion `copy_to_user`. La funcion `fifo_write` tiene un
>> comportamiento similar: primero se copian desde el espacio de usuario los
>> elementos que se desean escribir en el rbuffer dentro de un buffer temporal.
>> Luego se insertan los elementos del buffer temporal en el rbuffer.

> ### Operaciones opcionales

>> En nuestro fifoops.c también podemos implementar las funciones open 
>> (fifo_open) y release (fifo_release). La función fifo_open se encarga de 
>> asegurar que haya al menos un lector o al menos un escritor cada vez que se 
>> abre un fichero. El parámetro flags se comporta como un permiso a través de 
>> sus macros permitiendo asi saber cuando los ficheros deben ser abiertos para 
>> lectura (O_RDONLY), escritura (O_WRONLY) o ambas casos (O_RDWR); esto nos 
>> permite lograr que cuando haya permiso para lectura y no haya lectores 
>> entonces los incrementamos, en el caso de tener permiso para escritura ocurre 
>> lo mismo solo que en este caso debemos asegurarnos que al momento de escribir 
>> no haya ningun lector que ya haya empezado a leer. En esta implementacion 
>> hacemos uso del semáforo mutex ya que necesitamos de la exclusion mutua sino 
>> podria ocurrir una interrupcion y permitirse mas de un lector o escritor.
>> La función fifo_release se encarga de ir decrementando los lectores y 
>> escritores a tráves de los permisos para cerrar el fichero. En ésta función 
>> también se utiliza el semáforo mutex para evitar que al momento de decrementar 
>> los lectores y escritores no ocurra una interrupcion que cambie los valores 
>> esperados.
>> Al implementar dichas funciones nos vimos obligados a agregar variables 
>> globales como readers_count y writers_count que lleven la cuenta de cuantos 
>> lectores y escritores se estan ejecutando. Aprovechando la creación de estas 
>> variables creamos una función que nos imprime la información de estas y de 
>> otras variables mas como rbuffer_size (lleva la cuenta de cuantos elementos 
>> tiene el buffer), read_count (lleva la cuenta de cuanto se leyo hasta el 
>> momento) y operation (operacion que se esta ejecutando); de esta forma el 
>> usuario puede ir observando en que momento se acciona el lector, cuanto lee, 
>> cuando se acciona el escritor, cuanto escribe, que porcentaje del buffer se 
>> esta ocupando, cuando se desocupa y ocupa nuevamente, etc.



> ### Sincronizacion

>> Se decidio usar 3 semaforos: el primero fue inicializado en 0 (la cantidad de 
>> elementos dentro del rbuffer). Se le aplica la operación V luego de insertar 
>> un elemento en el rbuffer y la operación P antes de leer un elemento del 
>> rbuffer. De este modo, el semáforo indica la cantidad de elementos que estan 
>> actualmente en el rbuffer y cuando se quiera extraer un elemento se tendra 
>> que esperar a que se inserte uno nuevo. El segundo semáforo fue inicializado 
>> en size que es la cantidad máxima de elementos que puede alojar el rbuffer. 
>> El semáforo decrementa antes de insertar un nuevo elemento e incrementa 
>> cuando se extrae un elemento del rbuffer. De este modo nunca se escribe en el 
>> rbuffer si éste está lleno. Por último el tercer semáforo mutex fue 
>> inicializado en 1 para indicar que esta disponible al momento de ser llamado, 
>> este semáforo se encarga de asegurar la exclusión mutua, es decir, que cierta 
>> zona del código llamada zona crítica, no sea interrumpida hasta que termine 
>> de ejecutarse para evitar la alteración de nuestros valores por algun otro 
>> proceso.

Problemas encontrados
---------------------

+ En un principio, cuando se deseaba insertar el modulo, resultaba en una
  pantalla negra de error. Esto fue debido a mal manejo de punteros y fue
  solucionado sin mayores problemas. Debido a esto nos dimos cuenta que
  programar en espacio de kernel es un poco mas serio que en espacio de
  usuario.

+ En uno de las primeras implementaciones, nos dimos cuenta que si asignabamos
  al parametro de entrada  `size` un valor mayor o igual a 2^22 no se podia
  crear el ringbuffer. Suponemos que esto es debido a que en nuestros sistemas
  el limite de posible espacio a alojar por la funcion `kmalloc` debe estar
  configurada en ese valor. Teniendo en cuenta que el limite maximo definido
  en `slab.h` para `kmalloc` es, por defecto, de 2^25 [1].
  Esto sirvio para darnos cuenta de que no desregistrabamos el dispositivo si
  el modulo terminaba prematuramente con codigo de error, lo que causaba que
  el major number utilizado quedara ocupado.

Bibliografia
------------

> [0] Programacion de Controladores para Linux - E. Hames; J. Blaco; F. Luque  
> [1] [http://lxr.free-electrons.com/source/include/linux/slab.h#L180
      ](http://lxr.free-electrons.com/source/include/linux/slab.h#L180)  
