#ifndef _FRAGFAT_H
#define _FRAGFAT_H


typedef struct fat_stat_s fat_stat;


fat_stat *new_fat_stat(void);
/*
 * Crea una nueva structura 'fat_stat' vacia.
 * Recordar liberar los recursos asociados.
 */

fat_stat *get_fat_stat(char *fileaddr, fat_stat *stat);
/*
 * Parsea el FAT filesystem y devuelve un puntero a la estructura
 * con los datos obtenidos.
 *     fileaddr: direccion del archivo con el FAT filesystem
 *     stat: puntero a una estructura fat_stat
 */

unsigned int get_fragments_count(char *fileaddr, fat_stat *stat,
                                 unsigned int clusterstart);
/*
 * Devuelve la cantidad de fragmentos para el archivo que empieza en el
 * cluster 'clusterstart'
 *     fataddr: direccion de la primera tabla fat
 *     clusterstart: numero de cluster en el cual comienza el archivo
 */

float get_ratio_wiki(char *fileaddr, fat_stat *stat);
/*
 * Calcula el indice de fragmentacion externa segun la wikipedia:
 *      FragExterna = (1 - largest_free_block_of_mem / free_mem)
 *
 * Donde
 *     fataddr: direccion absoluta de la primera tabla fat
 *     fate: numbero de entradas en la fat
 */

float get_ratio_catedra(char *fileaddr, fat_stat *stat);
/*
 * Devuelve el indice de fragmentacion sugerido por la catedra que es
 * la cantidad de fragmentos por KiB promedio del filesystem.
 * Osea:
 *     (sumatoria(fragmentos_de_archivo_i / tamano_archivo_i en KiB)) / N
 *       i=1..N
 * con N=total_de_archivos
 *
 * rootaddr: direccion absoluta del rootdir del filesystem en el archivo
 */

void print_output(float frag_wiki, float frag_cate);
/*
 * Imprime un simple output por stdout con los indices de fragmentacion.
 *     frag_wiki: indice de fragmentacion externa segun wikipedia
 *     frag_cate: indice de fragmentacion segun la catedra
 */

void plot(float frag_wiki, float frag_cate);
/*
 * Genera un archivo simple con los resultados y una imagen con los mismos
 * datos graficados.
 *     frag_wiki: indice de fragmentacion externa segun wikipedia
 *     frag_cate: indice de fragmentacion segun la catedra
 */


#endif
