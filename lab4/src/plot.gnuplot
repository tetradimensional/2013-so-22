reset
set term png truecolor
set output "plot.png"
set ylabel "%"
set yrange [0:1]
set boxwidth 0.40 relative
plot "data.dat" using 2:xticlabels(1) with boxes fs solid 0.5 title "fragmentacion"

