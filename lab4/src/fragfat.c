#include <assert.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "fragfat.h"

/* http://en.wikipedia.org/wiki/File_Allocation_Table#BIOS_Parameter_Block */
#define BPB_OFFSET 0x00b
#define RLS_OFFSET (BPB_OFFSET + 0x03)
#define BLKS_OFFSET (BPB_OFFSET + 0x08)
#define BLK_SIZE_OFFSET (BPB_OFFSET + 0x00)
#define CLUSTER_BLKS_OFFSET (BPB_OFFSET + 0x02)
#define FAT_BLKS_OFFSET (BPB_OFFSET + 0x0b)
#define FAT_COUNT_OFFSET (BPB_OFFSET + 0x05)
#define FAT_ENTRY_SIZE 12

/* http://en.wikipedia.org/wiki/File_Allocation_Table#Directory_entry */
#define ROOTDIR_ATTR_OFFSET 0x0b
#define ROOTDIR_EMPTY_ENTRY 0xe5
#define ROOTDIR_ENTRIES_OFFSET (BPB_OFFSET + 0x06)
#define ROOTDIR_ENTRY_SIZE 32
#define ROOTDIR_FILESIZE_OFFSET 0x1c
#define ROOTDIR_FILE_ENTRY 0x20
#define ROOTDIR_STARTCLUSTER_OFFSET 0x1a
#define ROOTDIR_VFAT_ENTRY 0x0f

/* http://en.wikipedia.org/wiki/File_Allocation_Table#File_Allocation_Table */
#define END_CLUSTER 0xfff
#define FREE_CLUSTER 0x000


/*
 * Estructura
 */

struct fat_stat_s {
    unsigned int total_blocks;
    unsigned int total_block_size;
    unsigned int total_clusters;
    unsigned int total_cluster_size;
    unsigned int total_size;
    unsigned int fat_offset;
    unsigned int fat_entries;
    unsigned int fat_size;
    unsigned int rootdir_offset;
    unsigned int rootdir_entries;
    unsigned int rootdir_size;
    unsigned int data_offset;
    unsigned int data_clusters;
    unsigned int data_size;
};


/*
 * Funciones principales
 */

fat_stat *new_fat_stat(void) {
/*
 * Crea una nueva structura 'fat_stat' vacia.
 * Recordar liberar los recursos asociados.
 */
    fat_stat *result = NULL;
    result = calloc(1, sizeof(struct fat_stat_s));

    assert(result != NULL);
    return result;
}

fat_stat *get_fat_stat(char *fileaddr, fat_stat *stat) {
/*
 * Parsea el FAT filesystem y devuelve un puntero a la estructura
 * con los datos obtenidos.
 *     fileaddr: direccion del archivo con el FAT filesystem
 *     stat: puntero a una estructura fat_stat
 */
    uint16_t blocks = 0;
    uint16_t blksize = 0;
    uint16_t fatblks = 0;
    uint16_t rls = 0;
    uint16_t rootentries = 0;
    uint8_t nfats = 0;
    uint8_t clusterblks = 0;
    unsigned int fatsize = 0;
    unsigned int fatoff = 0;
    assert(fileaddr != NULL);
    assert(stat != NULL);

    blocks = *((uint16_t *) (fileaddr + BLKS_OFFSET));
    blksize = *((uint16_t *) (fileaddr + BLK_SIZE_OFFSET));
    rls = *((uint16_t *) (fileaddr + RLS_OFFSET));
    fatblks = *((uint16_t *) (fileaddr + FAT_BLKS_OFFSET));
    nfats = *((uint8_t *) (fileaddr + FAT_COUNT_OFFSET));
    rootentries = *((uint16_t *) (fileaddr + ROOTDIR_ENTRIES_OFFSET));
    clusterblks = *((uint8_t *) (fileaddr + CLUSTER_BLKS_OFFSET));
    fatsize = fatblks*blksize;
    fatoff = rls*blksize;

    stat->total_blocks = blocks;
    stat->total_block_size = blksize;
    stat->total_clusters = blocks / clusterblks;
    stat->total_cluster_size = clusterblks * blksize;
    stat->total_size = blocks * blksize;
    stat->fat_offset = fatoff;
    stat->fat_entries = (fatsize*8) / FAT_ENTRY_SIZE;  /* byte == 8 bits */
    stat->fat_size = fatsize;
    stat->rootdir_offset = fatoff + nfats*fatsize;
    stat->rootdir_entries = rootentries;
    stat->rootdir_size = rootentries*ROOTDIR_ENTRY_SIZE;
    stat->data_offset = stat->rootdir_offset + stat->rootdir_size;
    stat->data_size = stat->total_size -
        (stat->rootdir_offset + stat->rootdir_size);
    stat->data_clusters = stat->data_size / stat->total_cluster_size;

    return stat;
}

unsigned int get_fragments_count(char *fileaddr, fat_stat *stat,
                                 unsigned int clusterstart) {
/*
 * Devuelve la cantidad de fragmentos para el archivo que empieza en el
 * cluster 'clusterstart'
 *     fataddr: direccion de la primera tabla fat
 *     clusterstart: numero de cluster en el cual comienza el archivo
 */
    char *addr = NULL;
    char byte0 = 0;
    char byte1 = 0;
    char byte2 = 0;
    int16_t entry = 0;
    int16_t entry0 = 0;
    int16_t entry1 = 0;
    unsigned int fragments = 0;
    assert(fileaddr != NULL);
    assert(stat != NULL);

    if (clusterstart != 0) {
        do {
            addr = fileaddr + stat->fat_offset + (3*(clusterstart/2));
            byte0 = addr[0] & 0xff;
            byte1 = addr[1] & 0xff;
            byte2 = addr[2] & 0xff;
            entry0 = (byte0 | 0xff00) & ((byte1 << 8) | 0xff);
            entry1 = byte2 << 4 | (byte1 & 0xf0) >> 4;
            entry = (clusterstart % 2 == 0) ? entry0 : entry1;
            entry &= 0xfff;
            if (entry != (int) clusterstart+1 && entry != END_CLUSTER) {
                fragments++;  /* si el bloque no es adyacente */
            }
            clusterstart = entry;
        } while (entry != END_CLUSTER);
    }

    return fragments;
}

float get_ratio_wiki(char *fileaddr, fat_stat *stat) {
/*
 * Calcula el indice de fragmentacion externa segun la wikipedia:
 *      FragExterna = (1 - largest_free_block_of_mem / free_mem)
 *
 * Donde
 *     fataddr: direccion absoluta de la primera tabla fat
 *     fate: numbero de entradas en la fat
 */
    char *fataddr = 0;
    char byte0 = 0;
    char byte1 = 0;
    char byte2 = 0;
    uint16_t entry0 = 0;
    uint16_t entry1 = 0;
    unsigned int freec = 0;
    unsigned int freecmax = 0;
    unsigned int tmp = 0;
    unsigned int clustcount = 0;
    unsigned int clusters = 0;
    assert(fileaddr != NULL);
    assert(stat != NULL);

    clusters = stat->data_clusters;
    fataddr = fileaddr + stat->fat_offset + 3; /* primeros 2 no son clusters */
    for (clustcount = 0; clustcount < clusters; fataddr+=3, clustcount+=2) {
        byte0 = fataddr[0];
        byte1 = fataddr[1];
        byte2 = fataddr[2];
        entry0 = byte0 | byte1 << 8;
        entry1 = byte2 << 4 | byte1 >> 4;

        if ((entry0 & 0xfff) == FREE_CLUSTER) {
            freec++;
            tmp++;
        } else {
            if (tmp > freecmax) {
                freecmax = tmp;
            }
            tmp = 0;
        }

        if (clustcount+1 < clusters) {
            if ((entry1 & 0xfff) == FREE_CLUSTER) {
                freec++;
                tmp++;
            } else {
                if (tmp > freecmax) {
                    freecmax = tmp;
                }
                tmp = 0;
            }
        }
    }

    if (tmp > freecmax ) {
        freecmax = tmp;
    }

    if (freec == 0) {
        return 0;
    } else {
        return (1 - (float) freecmax/(float) freec);
    }
}

float get_ratio_catedra(char *fileaddr, fat_stat *stat) {
/*
 * Devuelve el indice de fragmentacion sugerido por la catedra que es
 * la cantidad de fragmentos por KiB promedio del filesystem.
 * Osea:
 *     (sumatoria(fragmentos_de_archivo_i / tamano_archivo_i en KiB)) / N
 *       i=1..N
 * con N=total_de_archivos
 *
 * rootaddr: direccion absoluta del rootdir del filesystem en el archivo
 */
    float result = 0;
    unsigned int fragments = 0;
    unsigned int totalfiles = 0;
    uint32_t filesize = 0;
    uint16_t startclust = 0;
    char *addr = NULL;
    char *endrootdir = NULL;
    int i = 0;
    assert(fileaddr != NULL);
    assert(stat != NULL);

    addr = fileaddr + stat->rootdir_offset;
    endrootdir = addr + stat->rootdir_size;
    while (addr < endrootdir) {
        if ((*addr & 0xff) != 0x00 && (*addr & 0xff) != ROOTDIR_EMPTY_ENTRY &&
            (*(addr + ROOTDIR_ATTR_OFFSET) & 0xff) == ROOTDIR_FILE_ENTRY)
        {
            filesize = *((uint32_t *) (addr + ROOTDIR_FILESIZE_OFFSET));
            startclust = *((uint16_t *) (addr + ROOTDIR_STARTCLUSTER_OFFSET));
            if (startclust != 0) {
                fragments = get_fragments_count(fileaddr, stat, startclust);
                result += ((float) fragments / (float) filesize);
            }
            totalfiles++;
        }

        addr += ROOTDIR_ENTRY_SIZE;
        i++;
    }

    /* devolver promedio de fragmentos por KiB */
    if (totalfiles == 0) {
        return 0;
    } else {
        return ((float) result / (float) totalfiles) * 1024;
    }
}

void print_output(float frag_wiki, float frag_cate) {
/*
 * Imprime un simple output por stdout con los indices de fragmentacion.
 *     frag_wiki: indice de fragmentacion externa segun wikipedia
 *     frag_cate: indice de fragmentacion segun la catedra
 */
    printf("rwiki:    %f\n", frag_wiki);
    printf("rcatedra: %f\n", frag_cate);
}


void plot(float frag_wiki, float frag_cate) {
/*
 * Genera un archivo simple con los resultados y una imagen con los mismos
 * datos graficados.
 *     frag_wiki: indice de fragmentacion externa segun wikipedia
 *     frag_cate: indice de fragmentacion segun la catedra
 */
    FILE *f = NULL;

    f = fopen("data.dat", "w");
    if (f == NULL) {
        perror("datafile");
    } else {
        fprintf(f, "fragwiki %f \nfragcatedra %f\n", frag_wiki, frag_cate);
        fclose(f);
        system("gnuplot plot.gnuplot");
    }
}
