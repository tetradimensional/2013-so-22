#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>

#include "fragfat.h"

#define FAT1_OFFSET 0x200
#define handle_error(msg) \
    do { perror(msg); exit(EXIT_FAILURE); } while (0)


int main(int argc, char *argv[])
{
    char *fileaddr = NULL;
    fat_stat *stat = NULL;
    float rcatedra = 0;
    float rwiki = 0;
    int fd = 0;
    struct stat s;

    /* setear todo lo necesario */
    if (argc < 2) {
        fprintf(stderr, "Usage: %s <file>\n", argv[0]);
        exit(EXIT_FAILURE);
    }
    if ((fd = open(argv[1], O_RDWR)) == -1) {
        handle_error("open");
    }
    if (fstat(fd, &s) == -1) {
        handle_error("stat");
    }
    fileaddr = mmap(NULL, s.st_size, PROT_READ|PROT_WRITE, MAP_SHARED, fd, 0);
    if (fileaddr == MAP_FAILED) {
        handle_error("map");
    }

    /* parsear datos necesarios */
    stat = new_fat_stat();
    stat = get_fat_stat(fileaddr, stat);

    /* calcular la fragmentacion */
    rwiki = get_ratio_wiki(fileaddr, stat);
    rcatedra = get_ratio_catedra(fileaddr, stat);

    /* mostrar */
    print_output(rwiki, rcatedra);
    plot(rwiki, rcatedra);

    /* limpiar y salir */
    if (munmap(fileaddr, s.st_size) == -1) {
        handle_error("munmap");
    }
    free(stat);
    close(fd);
    return 0;
}
