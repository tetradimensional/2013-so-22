#!/bin/bash

# Programita que ejecuta dos posibles casos de prueba para los indices
# de fragmentacion usados en este proyecto. Los datos generados fueron
# usados para generar los graficos en la presentacion.
#
# rwiki: La idea es llenar al 100% un sistema de archivos FAT12 generico
#        con archivos lo mas discontinuados posibles. Luego ir eliminando
#        cada uno de ellos y medir el nivel de fragmentacion externa con el
#        indice rwiki (ratio wikipedia). ->
#        http://en.wikipedia.org/wiki/Fragmentation_%28computing%29#Overview
#
# rcatedra: La idea es llenar (no necesariamente al 100%) varios sistemas
#           de archivos FAT12 variando en cada uno el tamaño de cluster.
#           Luego medir el nivel del "fragmentacion por discontinuidades"
#           con el indice rcatedra (ratio catedra). Este indice se puede
#           encontrar en las ultimas filminas de la presentacion
#           correspondiente al este proyecto.


FILE_NAMES=testfile
MOUNT_POINT=mnt
PROGRAM_NAME=fragfat
DISCONT=${3:-10}
SECTOR_SIZE=${2:-512}


function rcatedra_()
{
    for i in 1 2 4 8; do
        FILE="$FILE_NAMES$i.fs"
        echo ">>> $FILE"

        # crear archivos de 1MiB con FAT12 fs
        dd if=/dev/zero of="$FILE" bs=1024 count=1024
        mkdosfs -F 12 -s "$i" "$FILE"  # -s significa sectores

        # montar
        mount "$FILE" "$MOUNT_POINT"
        cd "$MOUNT_POINT"

        # generar 10 archivos con $DISCONT discontinuidades
        for j in `seq 0 $DISCONT`; do
            for k in {0..9}; do
                printf "$k%.s" `seq 1 $(($i*$SECTOR_SIZE))` >> "$k.txt"
            done
        done

        # medir
        cd ..
        umount "$MOUNT_POINT"
        "./$PROGRAM_NAME" "$FILE"
        printf "\n"
    done
}

function rwiki_()
{
    local SECT_CLUST=4
    local MAX_DISCONT=50

    FILE="$FILE_NAMES.fs"
    echo ">>> $FILE"

    # crear archivo de 1MiB con FAT12 fs
    dd if=/dev/zero of="$FILE" bs=1024 count=1024
    mkdosfs -F 12 -s "$SECT_CLUST" "$FILE"  # -s significa sectores

    # montar
    mount "$FILE" "$MOUNT_POINT"
    cd "$MOUNT_POINT"

    # llenar todo el sistema de archivos con archivos discontinuados
    for i in `seq 0 $MAX_DISCONT`; do
        for j in {0..9}; do
            printf "$j%.s" `seq 1 $(($SECT_CLUST*$SECTOR_SIZE))` >> "$j.txt" \
                2> /dev/null
        done
    done

    # borrar cada archivo y medir
    cd ..
    umount -l "$MOUNT_POINT"
    "./$PROGRAM_NAME" "$FILE"
    printf "\n"
    for i in {0..9}; do
        mount "$FILE" "$MOUNT_POINT"
        cd "$MOUNT_POINT"
        rm "$i.txt"
        cd ..
        umount -l "$MOUNT_POINT"
        "./$PROGRAM_NAME" "$FILE"
        printf "\n"
    done
}

function usage_()
{
    printf "Uso: sudo $0 <rwiki|rcatedra> [clustersize] [discontinuities]\n"
    printf "\n"
    printf "Programa que genera los datos vistos en los graficos de la\n"
    printf "presentacion mediante dos posibles casos de prueba.\n"
}


# Funcion main

if [[ "$UID" != 0 || ("$1" != "rwiki" && "$1" != "rcatedra") ]]; then
    usage_
else
    umount "$MOUNT_POINT" 2> /dev/null
    mkdir -p "$MOUNT_POINT"
    make
    printf "\n"

    if [[ "$1" == "rcatedra" ]]; then
        rcatedra_
    elif [[ "$1" == "rwiki" ]]; then
        rwiki_
    fi

    rmdir "$MOUNT_POINT"
fi
